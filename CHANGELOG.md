Changelog
============

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

Unreleased
************

7.3.1
*******
* Allow user to specify their own password secret in the values.yaml instead of only using the default one deployed by the umbrella

7.3.0
************
* Secret management changes: Use a VaultStaticSecret resource rather than the SecretProviderClass and define 
  the Secret resource in the umbrella chart with the postgres deployment
* Raise custom errors from the persistence layer rather than Python built ins. See ska_db_oda.persistence.errors for new types.
* Update the Helm charts so that they have better defaults and require minimal changes in the makefile. See the docs
  Deployment > Configuration page.

7.2.0
************
* Bump PDM version to 17.1.0 (from 17.0.0)

7.1.1
********
* [BREAKING] Renamed column operator_name to user_name in annotation table for SLT (Shift Log Tool)

7.1.0
********
* Added annotation table for SLT(Shift Log Tool)

7.0.0
********
* Added XRAY configuration

7.0.0 RC
***********
* Updated to PDM v17.0.0
* Used PDM builders for SBDefintions in tests rather than json files in this repo

6.3.1
***********
* Changed PDM to v16.1.0

6.3.0
***********
* Added Tables for SLT(Shift Log Tool)

6.2.3
***********
* Remove definition of oauth secret in ska-db-oda helm chart that pulls the oauth secret from Vault.

6.2.2
***********
* Reinstate FlaskODA object but converted to FastAPI, to fix issue where connection pool was starting before postgres was ready

6.2.1
***********
* Changed PDM to v16.0.1

6.2.0
***********
* Changed PDM to v16.0.0
* Added oauth secret in ska-db-oda helm chart that pulls the oauth secret from Vault.

6.1.0
******
* Define secret in ska-db-oda helm chart that pulls the postgres password from Vault. Use this k8s secret for the postgres and pgadmin passwords, 
  rather than have their charts create their own.
* Convert from Flask application into FastAPI. Involved splitting the resources.py with into separate modules for each resource.
* Added version ref for status entities, which links to the entity version
* Update to PDM v15.4.0

6.0.0
******

* [BREAKING] Removed `rest` and `memory` implementation of Repository
* [BREAKING] Moved the `unitofwork`, `repository` and `domain` top level packages into a `persistence` package
* ODA CLI client now works with either the local filesystem (i.e. the filesystem where the client is installed) or
  a remote ODA API depending on whether `ODA_CLIENT_USE_FILESYSTEM` env variable is True or False/unset.


5.3.3
*****

* [BUGFIX] Added workaround for PUT api url for status entity.


5.3.2
*****

* [BUGFIX] Fixed PUT api for status entity.


5.3.1
*****

* [BUGFIX] Fixed rest layer support for status entity PUT api.
* Updated Enum values in capitalize for status entity.


5.3.0
*****

* Updates ska-oso-pdm dependency to v15.


5.2.0

*****

* Added REST API support to update status for project entity and to get status history for the entity.
* Update 'cycle' field for proposal in table and 'elevation' in testfile


5.1.0

*****

* Add Helm value `pgadmin4.env.listenAddress` which will set the environment variable PGADMIN_LISTEN_ADDRESS in the pgadmin pod. 
  If this value is left to null then the default pgadmin value will be used.


5.0.1

*****

* Unpins PDM from exact match on v14.0.1 to allow use of ODA as a library in other applications. 


5.0.0

*****

* [BREAKING] Updates to ska-oso-pdm v14.0.1 from 13.0.1. This is a breaking change for the ODA as the PDM objects are the request bodies of the ODA API. See PDM change log for details on model changes.
* Uncommented test cases for Proposal in component test and integration test
* Added test cases for Proposal in unit test

4.1.0

*****

* Added REST API for easy retrieval of related ODA entities with only an Execution Block ID 

4.0.0
*****

* [BREAKING] Updates to ska-oso-pdm v13.0.0 from 12.0.1. This is a breaking change for the ODA as the PDM objects are the request bodies of the ODA API. See PDM change log for details on model changes.
* [BREAKING] Added REST API support to update status for entity like SBD,SBI and EB and to get status history for the entity.


3.0.2
*****

* [BUGFIX] Updated to latest make submodule. Previous 3.0.1 patch was not published correctly due to bug in makefiles.


3.0.1
*****

* [BUGFIX] Fixed bug where error handling tried to access error.args even if there were none, resulting in a useless error message.

3.0.0
*****

* [BREAKING] Updates to ska-oso-pdm v12.0.1 from 11.4.0. This is a breaking change for the ODA as the PDM objects are the request bodies of the ODA API. See PDM change log for details on model changes.
* [BREAKING] The Postgres implementation of Repository.query now returns a list of the PDM entity, rather than a dict which has the PDM entity in the 'info' entry.
* [BREAKING] Updated the Postgres tables to use ``timestamp with time zone`` rather than ``timestamp``