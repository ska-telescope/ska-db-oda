.. _oda_entities:

ODA Entities
=============

The ODA stores all of the data entities relevant to OSO. These top level entities are ``Proposal``, ``Project``, ``SBDefinition``, ``SBInstance`` and ``ExecutionBlock``.

These entities are defined in the PDM, and more information can be found in the `PDM RTD <https://developer.skao.int/projects/ska-oso-pdm/en/latest/>`_

Status Entities
----------------

OSO entities have an associated state; for example, SBIs could be ``CREATED``, ``IN PROGRESS``, ``OBSERVED``, ``FAILED``. 
`ADR-41 <https://confluence.skatelescope.org/display/SWSI/ADR-41+Architectural+Location+for+Execution+Blocks>`_ formalises the relationship between entities and status entities, showing several data entities (projects, SBDs, SBIs, etc.) 
to have a 1:1 relationship with an associated status entity.

PostgreSQL Tables Structure
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Created separate tables for SBD, SBI and EB entities. ``tab_oda_sbd_status_history``, ``tab_oda_sbi_status_history``, 
``tab_oda_eb_status_history`` and ``tab_oda_prj_status_history``.  

`For More Detailed info Click <https://confluence.skatelescope.org/pages/viewpage.action?pageId=267867440>`_


.. note::

    In status history tables composite key is combination of ``id``, ``entity_ref`` with ``{ENTITY_NAME}_version`` and ``current_status``.
    To allow duplicate values of ``id`` and ``entity_ref`` with ``{ENTITY_NAME}_version`` and ``current_status`` where ENTITY_NAME is eb, sbi, 
    sbd and prj

File System Structure
~~~~~~~~~~~~~~~~~~~~~

For Status history new file will be created in same location as json file with name ``status_history_{version}.json``

.. figure:: ../diagrams/New_File_Structure.png
   :align: center

Curl APIs
~~~~~~~~~

For getting latest status history data from ODA Entities

.. code-block:: console

   $ curl -iX GET -H -d  "https://k8s.stfc.skao.int/staging-ska-db-oda/oda/api/<MAJOR_VERSION>/status/ebs/eb-mvp01-20240426-5781"

.. code-block:: console

   $ curl -iX GET -H -d  "https://k8s.stfc.skao.int/staging-ska-db-oda/oda/api/<MAJOR_VERSION>/status/sbis/sbi-mvp01-20240426-5782"

.. code-block:: console

   $ curl -iX GET -H -d  "https://k8s.stfc.skao.int/staging-ska-db-oda/oda/api/<MAJOR_VERSION>/status/sbds/sbd-mvp01-20240426-5783"

.. code-block:: console

   $ curl -iX GET -H -d  "https://k8s.stfc.skao.int/staging-ska-db-oda/oda/api/<MAJOR_VERSION>/status/prjs/prj-mvp01-20240426-5783"


For getting all status history data from ODA Entities

.. code-block:: console

   $ curl -iX GET -H -d  "https://k8s.stfc.skao.int/staging-ska-db-oda/oda/api/<MAJOR_VERSION>/status/history/ebs/eb-mvp01-20240426-5781"

.. code-block:: console

   $ curl -iX GET -H -d  "https://k8s.stfc.skao.int/staging-ska-db-oda/oda/api/<MAJOR_VERSION>/status/history/sbis/sbi-mvp01-20240426-5782"

.. code-block:: console
   
   $ curl -iX GET -H -d  "https://k8s.stfc.skao.int/staging-ska-db-oda/oda/api/<MAJOR_VERSION>/status/history/sbds/sbd-mvp01-20240426-5783"

.. code-block:: console
   
   $ curl -iX GET -H -d  "https://k8s.stfc.skao.int/staging-ska-db-oda/oda/api/<MAJOR_VERSION>/status/history/prjs/prj-mvp01-20240426-5783"