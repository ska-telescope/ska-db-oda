.. _module_view:

Module View
============

The ``ska-db-oda`` project defines 3 main packages:

    1. ``persistence`` defines the Repository and UnitOfWork abstractions that are used for access by OSO applications.
    2. ``rest`` defines a FastAPI server which will provide an RESTful API for use by clients external to OSO, and uses the ``persistence`` layer to access the database.
    3. ``client`` defines some helper modules that make interacting with the RESTful API easier, and also an installable CLI.

.. note::
   A top level module view will be added after any refactoring done in BTN-2414

Unit Of Work Class Diagram
---------------------------

.. figure:: ../diagrams/Unit_Of_Work_Class_Diagram.svg
   :align: center

Repository Class Diagram
-------------------------

.. figure:: ../diagrams/Repository_Class_Diagram.svg
   :align: center
