.. _implementaton:

Implementation
===============

The ODA consists of two main elements:

1. The database layer, made up of a database instance or cluster.
2. The access layer that, depending on a client, is either a library to be imported or a Python application offering a RESTful API.

.. figure:: ../diagrams/oso-oda.jpg
   :align: center

Database Layer
------------------

We have chosen to use Postgres for the main persistence. The ``ska-db-oda-umbrella`` chart will deploy an instance of Postgres and an instance
of pgadmin to Kubernetes. For more info on this see :doc:`postgresql` and :doc:`../deployment/deployment_to_kubernetes`.

The ODA can also be deployed to use the filesysem rather than a full database.

Access Layer
-------------

The interface to access the ODA is different depending on whether the client is another OSO application or an external service or user. In
either case, the source code for the access layer is in the ``ska-db-oda`` repository.

For access within OSO, the ``ska-db-oda`` Python wheel will be installed as a dependency to use the ``persistence`` package - see 'Internal OSO Interactions'.

``ska-db-oda`` is also deployable as a FastAPI server (soon to be FastAPI) with an API that can be used by external clients. The ``client`` package can
be installed by the clients to make use of helper functions to interact with the API - see 'Internal OSO Interactions'.

For more details on the packages and modules, see :doc:`module_view`

