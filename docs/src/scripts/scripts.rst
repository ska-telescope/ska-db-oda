.. _scripts:

Scripts
=======

The scripts folder, in the root of the project directory, houses scripts that are relative to the ODA but are not
appropriate for the `src/` directory.

test_data_generation.py
-----------------------
This script can be used to generate syntactically valid SBDefinitions (SBDs) that can potentially be used for testing
purposes. The script creates a specified number of SBDs and saves them in JSON format, either by creating a timestamped
folder outside of the project folder or into a specified location.

before running the script, first run

.. code-block:: console

    $ poetry install
    $ poetry shell

Followed by

.. code-block:: console

    $ python3 scripts/test_data_generation.py --n N --location /path/to/directory

both the command line inputs are optional e.g.

.. code-block:: console

    $ python3 scripts/test_data_generation.py

Will generate 10 test json files in a folder outside the ODA project folder with name e.g. `YYYYMMDDhhmmss_TZ`
where `TZ` is the system timezone e.g. CET.


.. code-block:: console

    $ python3 scripts/test_data_generation.py --n 100

Will generate 100 test json files in a folder outside the ODA project folder with name e.g. `YYYYMMDDhhmmss_TZ`.

.. code-block:: console

    $ python3 scripts/test_data_generation.py --n 100 --location 'repo'

Will generate 100 test json files in a folder in the root of the directory with name `repo`. If the folder already
exists then the files will be put into the existing folder. Nothing in the folder will be overwritten.

It is possible to create `ska_oso_pdm.PdmObjects` from these json files in python. Simply open a python terminal and
enter:

.. code-block:: console

    $ python3
    >>> from ska_oso_pdm import SBDefinition
    >>> from ska_oso_pdm.openapi import CODEC
    >>> CODEC.load_from_file(SBDefinition,'path/to/file.json')

