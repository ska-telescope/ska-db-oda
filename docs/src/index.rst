======================
SKA ODA Documentation
======================

The Observatory Science Operations Data Archive (ODA) is the central datastore for OSO.
It persists and provides access to all entities involved in the OSO subsystems (for example, Scheduling Blocks or Execution Blocks).

For an overview of the applications and functionality within OSO, see `Solution Intent <https://confluence.skatelescope.org/pages/viewpage.action?pageId=159387040>`_.

For information on deploying and configuring the application in a given Kubernetes or local environment, see the 'Deploying and Configuring' section.

For user information for a deployed instance of this application, see the 'User Guide'.

For developer information, application internals, and information about interactions with other OSO applications, see the 'Developer Information' section.

For instructions on developing the application, see the `README <https://gitlab.com/ska-telescope/db/ska-db-oda/-/blob/main/README.md>`_.


.. toctree::
   :maxdepth: 1
   :caption: Releases
   :hidden:

   CHANGELOG.rst


.. toctree::
    :maxdepth: 1
    :caption: General
    :hidden:

    general/oso_context.rst
    general/implementation.rst
    general/module_view.rst
    general/oda_entities.rst
    general/postgresql.rst

.. toctree::
    :maxdepth: 2
    :caption: Deploying and Configuring
    :hidden:

    deployment/deployment_to_kubernetes.rst
    deployment/configuration.rst
    deployment/persistent_environments.rst
    deployment/secret_management.rst

.. toctree::
    :maxdepth: 2
    :caption: User Guide
    :hidden:

    external/rest_api.rst
    external/cli.rst
    external/odahelper.rst

.. toctree::
    :maxdepth: 2
    :caption: Developer Information
    :hidden:

    internal/repository_and_uow/repository_and_uow.rst
    internal/oda_responsibility_and_expectations.rst


.. toctree::
    :maxdepth: 2
    :caption: Scripts
    :hidden:

    scripts/scripts.rst