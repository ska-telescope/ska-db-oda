.. _rest_api:

REST API
=========

The ``ska-db-oda`` application offers a RESTful API when deployed. This is primarily intended to be used by services
outside of OSO, and also to easily be able to upload and examine data during testing and comissioning.

The resource endpoints, with the accepted requests and expected responses, are documented via an OpenAPI specification at https://k8s.stfc.skao.int/integration-ska-db-oda/oda/api/v7/openapi.json
or as a Swagger UI at https://k8s.stfc.skao.int/integration-ska-db-oda/oda/api/v7/ui

The API supports resources for SBDefinitions, SBInstances, ExecutionBlocks, Projects and Proposals, as well as their status entities.
Each resource supports a POST method to create a new entity, which will fetch a new identifier from SKUID.
There are also GET and PUT method for a resource identifier to retrieve and update the entity, retrospectively.
There is also a general end point which will support PUT for multiple resources as an atomic operation.

Additionally, there are custom Execution Block resources. For more context, see :ref:`odahelper`.

Once deployed, the API should be available at ``<HOST>/oda/api/v<MAJOR_VERSION>/<RESOURCE>`` and the Swagger UI at ``<HOST>/api/v<MAJOR_VERSION>/ui``. The host depends on the environment that the server is deployed to, and may include a k8s namespace - see the README for more details. For example, to retrieve SBDefinition sbd-mvp01-20200325-00001 from the staging environment, the request would be

.. code-block:: console

   $ curl -iX GET -H -d  "https://k8s.stfc.skao.int/staging-ska-db-oda/oda/api/<MAJOR_VERSION>/sbds/sbd-mvp01-20200325-00001"

