.. _CLI:

ODA Command Line Interface (CLI)
================================

The ODA Command Line Interface package provides the user with a simple interface
for accessing and querying different entities stored in the ODA.

Currently supported entity types (followed by the CLI abbreviation) are:

- Scheduling Block Definitions (sbds)
- Scheduling Block Instances (sbis)
- Execution Blocks (ebs)
- Projects (prjs)
- Scheduling Block Definitions Status history (sbd_status_history)
- Scheduling Block Instances Status history (sbi_status_history)
- Execution Blocks Status history (eb_status_history)
- Projects Status history (prj_status_history)

Configuration
-------------

ODA CLI is installed as part of the ska-db-oda package:

.. code-block:: console

    $ pip install ska-db-oda --extra-index-url=https://artefact.skao.int/repository/pypi-internal/simple

The CLI can be configured to query an ODA API, or to use the local client filesystem. This filesystem functionality
is a useful tool for testing or notebooks. The following environment variables are used:

.. list-table:: Environment variables used by ODA CLI classes
   :widths: 23 40 30
   :header-rows: 1

   * - Environment variable
     - Description
     - Required/optional
   * - ODA_CLIENT_USE_FILESYSTEM
     - Set to ``True`` if the CLI should query the local filesystem, or ``False`` if it should query a remote ODA API.
     - Optional - default: ``False``
   * - ODA_URL
     - The API address of a running instance of the ODA to use, in the format ``http://<minikube_ip>/<kube_namespace>/oda/api/v<MAJOR_VERSION>``
     - Required if ``ODA_CLIENT_USE_FILESYSTEM`` is ``False``
   * - ODA_DATA_DIR
     - The base filesystem location where the client will query entities
     - Optional - default: ``/var/lib/oda``

.. note::
   The filesystem client expects entities to be stored in a specific location, which is of the format ``<ODA_DATA_DIR>/<entity>/<entity_id>/<entity_version>.json``
   For example, ``/var/lib/oda/sbd/sbd-mvp01-20240426-5783/2.json``. There are plans to make this more flexible in the future, as it is often useful to save an SBDefintion with
   some information about the type of observation in its name.

To ensure the CLI is available, and for more help, run ``oda --help``.

The typical use pattern is then as below, and specific examples can be seen in the following sections.

.. code-block:: console

    $ oda <entity abbreviation> <command> <parameters>

Commands
--------

+----------------+----------------------+------------------------------------------------+
| ODA CLI command| Parameters           | Description                                    |
+================+======================+===============+================================+
| get            | entity_id            | Get entity by ID, first specifying entity      |
|                |                      | type. Example:                                 |
|                |                      | oda sbis get sbi-mvp01-20200325-00001          |
| get            | associated_entity    | Get associated entity data by specifying       |
|                |                      | entity id. Example:                            |
|                |                      | oda ebs get 'eb-t0001-20240521-00003' 'sbds'   |
+----------------+----------------------+------------------------------------------------+
| query          |                      | Query ODA for entity IDs. The query can be     |
|                |                      | one of the following:                          |
|                +----------------------+------------------------------------------------+
|                | user                 | User query: Specify a name of the              |
|                |                      | creator of the entity                          |
|                |                      |                                                |
|                +----------------------+------------------------------------------------+                   
|                |                      | set starts_with to True if only wanting to     |                                     
|                | starts_with          | match the beginning of the user                |
|                |                      |                                                |
|                +----------------------+------------------------------------------------+
|                | contains             | set contains to True if only wanting to        |
|                |                      | match the pattern of the entity_id/user        |
|                +----------------------+------------------------------------------------+
|                | created_before       | Date created query: Specify a start and/or     |
|                +----------------------+ end date for when the entity was first created |
|                | created_after        |                                                |
|                +----------------------+------------------------------------------------+
|                | last_modified_before | Date modified query: Specify a start and/or    |
|                +----------------------+ end date for when the entity was last modified.|
|                | last_modified_after  |                                                |
|                +----------------------+------------------------------------------------+
|                | entity_id            | EntityID query: Specify a entity_id of the     |
|                |                      | creator of entity, set starts_with to True     |
|                |                      | if only wanting to match beginning of          |
|                |                      | the entity_id or set contains to True if       |
|                |                      | want to match the pattern.(False by default)   |
+----------------+----------------------+------------------------------------------------+

Examples
---------

A typical use case would be to query for entities created in a certain time window or by a certain user,
then to retrieve the full entity.

For example, to query all ExecutionBlocks created on a certain day

.. code-block:: console

 $ oda sbis query --created_after='2022-10-18 00:00' --created_before='2024-10-18 23:59'

    Query Parameters
    ---------------
    query_type = QueryType.CREATED_BETWEEN
    start = 2022-10-18 00:00:00
    end = 2024-10-18 23:59:00

    Result
    -------
    [
        {
            "interface": "https://schema.skao.int/ska-oso-pdm-sbi/0.1",
            "metadata": {
                "created_by": "DefaultUser",
                "created_on": "2023-10-07T16:28:41.314279+00:00",
                "last_modified_by": "DefaultUser",
                "last_modified_on": "2023-10-07T16:28:41.314279+00:00",
                "version": 1
            },
            "sbd_ref": "sbd-mvp01-20220923-00003",
            "sbd_version": 1,
            "sbi_id": "sbi-mvp01-20220923-00003",
            "subarray_id": 3,
            "telescope": "ska_mid"
        },
        {
            "interface": "https://schema.skao.int/ska-oso-pdm-sbi/0.1",
            "metadata": {
                "created_by": "DefaultUser",
                "created_on": "2023-10-03T18:26:50.416261+00:00",
                "last_modified_by": "DefaultUser",
                "last_modified_on": "2023-10-03T18:26:50.416261+00:00",
                "version": 1
            },
            "sbd_ref": "sbi-mvp01-20220923-00002",
            "sbd_version": 1,
            "sbi_id": "sbi-mvp01-20220923-00002",
            "subarray_id": 3,
            "telescope": "ska_mid"
        },
        {
            "activities": [
                {
                    "activity_ref": "test activity",
                    "executed_at": "2022-09-23T15:43:53.971548+00:00",
                    "runtime_args": [
                        {
                            "function_args": {
                                "kwargs": {
                                    "foo": "bar"
                                }
                            },
                            "function_name": "test_fn"
                        }
                    ]
                }
            ],
            "eb_ref": "eb-mvp01-20220923-00001",
            "interface": "https://schema.skao.int/ska-oso-pdm-sbi/0.1",
            "metadata": {
                "created_by": "DefaultUser",
                "created_on": "2023-10-18T17:02:53.611151+00:00",
                "last_modified_by": "DefaultUser",
                "last_modified_on": "2023-10-18T17:02:53.611151+00:00",
                "version": 1
            },
            "sbd_ref": "sbd-mvp01-20220923-00001",
            "sbd_version": 1,
            "sbi_id": "sbi-mvp01-20220923-00001",
            "subarray_id": 3,
            "telescope": "ska_mid"
        }
    ]

Then fetch the full ExecutionBlock for one of the IDs.

.. code-block:: console

 $ oda ebs get eb-t0001-20231018-00004

    {
        "interface": "https://schema.skao.int/ska-oso-pdm-eb/0.1",
        "eb_id": "eb-t0001-20231018-00004",
        "metadata": {
            "version": 1,
            "created_by": "DefaultUser",
            "created_on": "2023-10-18T08:36:46.710630+00:00",
            "last_modified_by": "DefaultUser",
            "last_modified_on": "2023-10-18T08:36:46.710630+00:00"
        },
        "telescope": "ska_mid",
        "sbd_ref": "sbd-mvp01-20220923-00001",
        "sbd_version": 1,
        "sbi_ref": "sbi-mvp01-20220923-00001",
        "request_responses": [
            {
              "request": "ska_oso_scripting.functions.devicecontrol.release_all_resources",
              "request_args": "{'args': None, 'kwargs': {'subarray_id': '1'}}",
              "status": "OK",
              "response": {
                "result": "this is a result"
              },
              "request_sent_at": "2022-09-23T15:43:53.971548+00:00",
              "response_received_at": "2022-09-23T15:43:53.971548+00:00"
            },
            {
              "request": "ska_oso_scripting.functions.devicecontrol.scan",
              "status": "ERROR",
              "error": {
                "detail": "this is an error"
              },
              "request_sent_at": "2022-09-23T15:43:53.971548+00:00"
            }
          ]
    }

Another example, to query SBInstances by user

.. code-block:: console

    $ oda sbis query -user='DefaultUser' --starts-with

        Query Parameters
        ---------------
        user = DefaultUser
        match_type = MatchType.STARTS_WITH

        Result
        -------
        [
          {
              "interface": "https://schema.skao.int/ska-oso-pdm-sbi/0.1",
              "metadata": {
                  "created_by": "DefaultUser",
                  "created_on": "2023-10-07T16:28:41.314279+00:00",
                  "last_modified_by": "DefaultUser",
                  "last_modified_on": "2023-10-07T16:28:41.314279+00:00",
                  "version": 1
              },
              "sbd_ref": "sbd-mvp01-20220923-00003",
              "sbd_version": 1,
              "sbi_id": "sbi-mvp01-20220923-00003",
              "subarray_id": 3,
              "telescope": "ska_mid"
          },
          {
              "interface": "https://schema.skao.int/ska-oso-pdm-sbi/0.1",
              "metadata": {
                  "created_by": "DefaultUser",
                  "created_on": "2023-10-03T18:26:50.416261+00:00",
                  "last_modified_by": "DefaultUser",
                  "last_modified_on": "2023-10-03T18:26:50.416261+00:00",
                  "version": 1
              },
              "sbd_ref": "sbi-mvp01-20220923-00002",
              "sbd_version": 1,
              "sbi_id": "sbi-mvp01-20220923-00002",
              "subarray_id": 3,
              "telescope": "ska_mid"
          },
          {
              "activities": [
                  {
                      "activity_ref": "test activity",
                      "executed_at": "2022-09-23T15:43:53.971548+00:00",
                      "runtime_args": [
                          {
                              "function_args": {
                                  "kwargs": {
                                      "foo": "bar"
                                  }
                              },
                              "function_name": "test_fn"
                          }
                      ]
                  }
              ],
              "eb_ref": "eb-mvp01-20220923-00001",
              "interface": "https://schema.skao.int/ska-oso-pdm-sbi/0.1",
              "metadata": {
                  "created_by": "DefaultUser",
                  "created_on": "2023-10-18T17:02:53.611151+00:00",
                  "last_modified_by": "DefaultUser",
                  "last_modified_on": "2023-10-18T17:02:53.611151+00:00",
                  "version": 1
              },
              "sbd_ref": "sbd-mvp01-20220923-00001",
              "sbd_version": 1,
              "sbi_id": "sbi-mvp01-20220923-00001",
              "subarray_id": 3,
              "telescope": "ska_mid"
          }
      ]

      $ oda sbis query --entity_id='sbi' --contains

        Query Parameters
        ---------------
        entity_id = sbi
        match_type = MatchType.CONTAINS

        Result
        -------
        [
          {
              "interface": "https://schema.skao.int/ska-oso-pdm-sbi/0.1",
              "metadata": {
                  "created_by": "DefaultUser",
                  "created_on": "2023-10-07T16:28:41.314279+00:00",
                  "last_modified_by": "DefaultUser",
                  "last_modified_on": "2023-10-07T16:28:41.314279+00:00",
                  "version": 1
              },
              "sbd_ref": "sbd-mvp01-20220923-00003",
              "sbd_version": 1,
              "sbi_id": "sbi-mvp01-20220923-00003",
              "subarray_id": 3,
              "telescope": "ska_mid"
          },
          {
              "interface": "https://schema.skao.int/ska-oso-pdm-sbi/0.1",
              "metadata": {
                  "created_by": "DefaultUser",
                  "created_on": "2023-10-03T18:26:50.416261+00:00",
                  "last_modified_by": "DefaultUser",
                  "last_modified_on": "2023-10-03T18:26:50.416261+00:00",
                  "version": 1
              },
              "sbd_ref": "sbi-mvp01-20220923-00002",
              "sbd_version": 1,
              "sbi_id": "sbi-mvp01-20220923-00002",
              "subarray_id": 3,
              "telescope": "ska_mid"
          },
          {
              "activities": [
                  {
                      "activity_ref": "test activity",
                      "executed_at": "2022-09-23T15:43:53.971548+00:00",
                      "runtime_args": [
                          {
                              "function_args": {
                                  "kwargs": {
                                      "foo": "bar"
                                  }
                              },
                              "function_name": "test_fn"
                          }
                      ]
                  }
              ],
              "eb_ref": "eb-mvp01-20220923-00001",
              "interface": "https://schema.skao.int/ska-oso-pdm-sbi/0.1",
              "metadata": {
                  "created_by": "DefaultUser",
                  "created_on": "2023-10-18T17:02:53.611151+00:00",
                  "last_modified_by": "DefaultUser",
                  "last_modified_on": "2023-10-18T17:02:53.611151+00:00",
                  "version": 1
              },
              "sbd_ref": "sbd-mvp01-20220923-00001",
              "sbd_version": 1,
              "sbi_id": "sbi-mvp01-20220923-00001",
              "subarray_id": 3,
              "telescope": "ska_mid"
          }
      ]


For fetching any entity status history data the output will be in list / array form.

.. code-block:: console

 $ oda eb_status_history query -entity-id="eb-mvp01-20240426-7785"

    [
        {
            "current_status": "Created",
            "eb_id": "eb-mvp01-20240426-7785",
            "eb_version": 1,
            "metadata": 
            {
                "created_by": "DefaultUser",
                "created_on": "2024-05-09T06:43:56.816443Z",
                "last_modified_by": "DefaultUser",
                "last_modified_on": "2024-05-09T06:43:56.816443Z"
            },
            "previous_status": "Created"
        },
        {
            "current_status": "Fully Observed",
            "eb_id": "eb-mvp01-20240426-7785",
            "eb_version": 1,
            "metadata": 
            {
                "created_by": "DefaultUser",
                "created_on": "2024-05-09T06:44:18.040906Z",
                "last_modified_by": "DefaultUser",
                "last_modified_on": "2024-05-09T06:44:18.040906Z"
            },
            "previous_status": "Created"
        }
    ]

For fetching any entity status data.

.. code-block:: console

 $ oda eb_status_history get eb-mvp01-20240426-7785

    {
        "current_status": "Fully Observed",
        "eb_id": "eb-mvp01-20240426-7785",
        "eb_version": 1,
        "metadata": 
        {
            "created_by": "DefaultUser",
            "created_on": "2024-05-09T06:44:18.040906Z",
            "last_modified_by": "DefaultUser",
            "last_modified_on": "2024-05-09T06:44:18.040906Z"
        },
        "previous_status": "Created"
    }

For example, Retrieving the Scheduling Block Definition using Execution Block

.. code-block:: console

 $ oda ebs get 'eb-t0001-20240521-00003' 'sbds'

    {
        "activities": {
            "allocate": {
            "function_args": {
                "init": {
                "args": [
                    "posarg1",
                    "posarg2"
                ],
                "kwargs": {
                    "argname": "argval"
                }
                },
                "main": {
                "args": [
                    "posarg1",
                    "posarg2"
                ],
                "kwargs": {
                    "argname": "argval"
                }
                }
            },
            "kind": "filesystem",
            "path": "file:///path/to/allocatescript.py"
            },
            "observe": {
            "branch": "main",
            "function_args": {
                "init": {
                "args": [
                    "posarg1",
                    "posarg2"
                ],
                "kwargs": {
                    "argname": "argval"
                }
                },
                "main": {
                "args": [
                    "posarg1",
                    "posarg2"
                ],
                "kwargs": {
                    "argname": "argval"
                }
                }
            },
            "kind": "git",
            "path": "git://relative/path/to/scriptinsiderepo.py",
            "repo": "https://gitlab.com/script_repo/operational_scripts"
            }
        },
        "csp_configurations": [
            {
            "cbf": {
                "fsps": [
                {
                    "channel_averaging_map": [
                    [
                        0,
                        2
                    ],
                    [
                        744,
                        0
                    ]
                    ],
                    "channel_offset": 0,
                    "frequency_slice_id": 1,
                    "fsp_id": 1,
                    "function_mode": "CORR",
                    "integration_factor": 1,
                    "output_link_map": [
                    [
                        0,
                        0
                    ],
                    [
                        200,
                        1
                    ]
                    ],
                    "zoom_factor": 0
                },
                {
                    "frequency_slice_id": 2,
                    "fsp_id": 2,
                    "function_mode": "CORR",
                    "integration_factor": 1,
                    "zoom_factor": 1,
                    "zoom_window_tuning": 650000
                }
                ]
            },
            "common": {
                "band_5_tuning": [
                5.85,
                7.25
                ],
                "subarray_id": 1
            },
            "config_id": "csp config 123",
            "subarray": {
                "subarray_name": "science period 23"
            }
            }
        ],
        "dish_allocations": {
            "receptor_ids": [
            "0002",
            "0001"
            ]
        },
        "dish_configurations": [
            {
            "dish_configuration_id": "dish config 123",
            "receiver_band": "5a"
            }
        ],
        "interface": "https://schema.skao.int/ska-oso-pdm-sbd/0.1",
        "metadata": {
            "created_by": "DefaultUser",
            "created_on": "2024-05-20T15:13:53.971548+05:30",
            "last_modified_by": "DefaultUser",
            "last_modified_on": "2024-05-20T19:09:04.112314+05:30",
            "version": 2
        },
        "sbd_id": "sbd-mvp01-20200325-00001",
        "scan_definitions": [
            {
            "csp_configuration_ref": "csp config 123",
            "dish_configuration_ref": "dish config 123",
            "pointing_correction": "MAINTAIN",
            "scan_definition_id": "calibrator scan",
            "scan_duration": 60000,
            "scan_type_ref": "calibration_B",
            "target_ref": "Polaris Australis"
            },
            {
            "csp_configuration_ref": "csp config 123",
            "dish_configuration_ref": "dish config 123",
            "pointing_correction": "UPDATE",
            "scan_definition_id": "science scan",
            "scan_duration": 60000,
            "scan_type_ref": "science_A",
            "target_ref": "M83"
            }
        ],
        "scan_sequence": [
            "calibrator scan",
            "science scan",
            "science scan",
            "calibrator scan"
        ],
        "sdp_configuration": {
            "execution_block": {
            "beams": [
                {
                "beam_id": "vis0",
                "function": "visibilities"
                },
                {
                "beam_id": "pss1",
                "function": "pulsar search",
                "search_beam_id": 1
                },
                {
                "beam_id": "pss2",
                "function": "pulsar search",
                "search_beam_id": 2
                },
                {
                "beam_id": "pst1",
                "function": "pulsar search",
                "timing_beam_id": 1
                },
                {
                "beam_id": "pst2",
                "function": "pulsar search",
                "timing_beam_id": 2
                },
                {
                "beam_id": "vlbi",
                "function": "vlbi",
                "vlbi_beam_id": 1
                }
            ],
            "channels": [
                {
                "channels_id": "vis_channels",
                "spectral_windows": [
                    {
                    "count": 744,
                    "freq_max": 368000000,
                    "freq_min": 350000000,
                    "link_map": [
                        [
                        0,
                        0
                        ],
                        [
                        200,
                        1
                        ],
                        [
                        744,
                        2
                        ],
                        [
                        944,
                        3
                        ]
                    ],
                    "spectral_window_id": "fsp_1_channels",
                    "start": 0,
                    "stride": 2
                    },
                    {
                    "count": 744,
                    "freq_max": 368000000,
                    "freq_min": 360000000,
                    "link_map": [
                        [
                        2000,
                        4
                        ],
                        [
                        2200,
                        5
                        ]
                    ],
                    "spectral_window_id": "fsp_2_channels",
                    "start": 2000,
                    "stride": 1
                    },
                    {
                    "count": 744,
                    "freq_max": 361000000,
                    "freq_min": 360000000,
                    "link_map": [
                        [
                        4000,
                        6
                        ],
                        [
                        4200,
                        7
                        ]
                    ],
                    "spectral_window_id": "zoom_window_1",
                    "start": 4000,
                    "stride": 1
                    }
                ]
                },
                {
                "channels_id": "pulsar_channels",
                "spectral_windows": [
                    {
                    "count": 744,
                    "freq_max": 368000000,
                    "freq_min": 350000000,
                    "spectral_window_id": "pulsar_fsp_channels",
                    "start": 0
                    }
                ]
                }
            ],
            "context": {
                "baz": 123,
                "foo": "bar"
            },
            "eb_id": "eb-mvp01-20200325-00001",
            "max_length": 100,
            "polarisations": [
                {
                "corr_type": [
                    "XX",
                    "XY",
                    "YY",
                    "YX"
                ],
                "polarisations_id": "all"
                }
            ],
            "scan_types": [
                {
                "beams": [
                    {
                    "beam_ref": "vis0",
                    "channels_ref": "vis_channels",
                    "polarisations_ref": "all"
                    },
                    {
                    "beam_ref": "pss1",
                    "channels_ref": "pulsar_channels",
                    "field_ref": "M83",
                    "polarisations_ref": "all"
                    },
                    {
                    "beam_ref": "pss2",
                    "channels_ref": "pulsar_channels",
                    "field_ref": "Polaris Australis",
                    "polarisations_ref": "all"
                    },
                    {
                    "beam_ref": "pst1",
                    "channels_ref": "pulsar_channels",
                    "field_ref": "M83",
                    "polarisations_ref": "all"
                    },
                    {
                    "beam_ref": "pst2",
                    "channels_ref": "pulsar_channels",
                    "field_ref": "Polaris Australis",
                    "polarisations_ref": "all"
                    },
                    {
                    "beam_ref": "vlbi",
                    "channels_ref": "vlbi_channels",
                    "field_ref": "Polaris Australis",
                    "polarisations_ref": "all"
                    }
                ],
                "scan_type_id": ".default"
                },
                {
                "beams": [
                    {
                    "beam_ref": "vis0",
                    "field_ref": "M83"
                    }
                ],
                "derive_from": ".default",
                "scan_type_id": ".default"
                }
            ]
            },
            "processing_blocks": [
            {
                "pb_id": "pb-mvp01-20200325-00001",
                "sbi_refs": [
                "sbi-mvp01-20200325-00001"
                ],
                "script": {
                "kind": "realtime",
                "name": "vis_receive",
                "version": "0.1.0"
                }
            },
            {
                "pb_id": "pb-mvp01-20200325-00002",
                "sbi_refs": [
                "sbi-mvp01-20200325-00001"
                ],
                "script": {
                "kind": "realtime",
                "name": "test_realtime",
                "version": "0.1.0"
                }
            },
            {
                "dependencies": [
                {
                    "kind": [
                    "visibilities"
                    ],
                    "pb_ref": "pb-mvp01-20200325-00001"
                }
                ],
                "pb_id": "pb-mvp01-20200325-00003",
                "sbi_refs": [
                "sbi-mvp01-20200325-00001"
                ],
                "script": {
                "kind": "batch",
                "name": "ical",
                "version": "0.1.0"
                }
            },
            {
                "dependencies": [
                {
                    "kind": [
                    "calibration"
                    ],
                    "pb_ref": "pb-mvp01-20200325-00003"
                }
                ],
                "pb_id": "pb-mvp01-20200325-00004",
                "sbi_refs": [
                "sbi-mvp01-20200325-00001"
                ],
                "script": {
                "kind": "batch",
                "name": "dpreb",
                "version": "0.1.0"
                }
            }
            ],
            "resources": {
            "csp_links": [
                1,
                2,
                3,
                4
            ],
            "receive_nodes": 10,
            "receptors": [
                "FS4",
                "FS8",
                "FS16",
                "FS17",
                "FS22",
                "FS23",
                "FS30",
                "FS31",
                "FS32",
                "FS33",
                "FS36",
                "FS52",
                "FS56",
                "FS57",
                "FS59",
                "FS62",
                "FS66",
                "FS69",
                "FS70",
                "FS72",
                "FS73",
                "FS78",
                "FS80",
                "FS88",
                "FS89",
                "FS90",
                "FS91",
                "FS98",
                "FS108",
                "FS111",
                "FS132",
                "FS144",
                "FS146",
                "FS158",
                "FS165",
                "FS167",
                "FS176",
                "FS183",
                "FS193",
                "FS200",
                "FS345",
                "FS346",
                "FS347",
                "FS348",
                "FS349",
                "FS350",
                "FS351",
                "FS352",
                "FS353",
                "FS354",
                "FS355",
                "FS356",
                "FS429",
                "FS430",
                "FS431",
                "FS432",
                "FS433",
                "FS434",
                "FS465",
                "FS466",
                "FS467",
                "FS468",
                "FS469",
                "FS470"
            ]
            }
        },
        "targets": [
            {
            "pointing_pattern": {
                "active": "FivePointParameters",
                "parameters": [
                {
                    "kind": "FivePointParameters",
                    "offset_arcsec": 5
                },
                {
                    "kind": "RasterParameters",
                    "n_rows": 2,
                    "pa": 7.89,
                    "row_length_arcsec": 1.23,
                    "row_offset_arcsec": 4.56,
                    "unidirectional": true
                },
                {
                    "kind": "StarRasterParameters",
                    "n_rows": 2,
                    "row_length_arcsec": 1.23,
                    "row_offset_angle": 4.56,
                    "unidirectional": true
                }
                ]
            },
            "radial_velocity": {
                "definition": "OPTICAL",
                "quantity": {
                "unit": "km / s",
                "value": -12.345
                },
                "redshift": 1.23,
                "reference_frame": "LSRK"
            },
            "reference_coordinate": {
                "dec": "-88:57:22.9",
                "kind": "equatorial",
                "ra": "21:08:47.92",
                "reference_frame": "icrs",
                "unit": [
                "hourangle",
                "deg"
                ]
            },
            "target_id": "Polaris Australis"
            },
            {
            "pointing_pattern": {
                "active": "SinglePointParameters",
                "parameters": [
                {
                    "kind": "SinglePointParameters",
                    "offset_x_arcsec": 0,
                    "offset_y_arcsec": 0
                }
                ]
            },
            "radial_velocity": {
                "definition": "RADIO",
                "quantity": {
                "unit": "km / s",
                "value": 0
                },
                "redshift": 0,
                "reference_frame": "LSRK"
            },
            "reference_coordinate": {
                "dec": "-29:51:56.74",
                "kind": "equatorial",
                "ra": "13:37:00.919",
                "reference_frame": "icrs",
                "unit": [
                "hourangle",
                "deg"
                ]
            },
            "target_id": "M83"
            }
        ],
        "telescope": "ska_mid"
    }
