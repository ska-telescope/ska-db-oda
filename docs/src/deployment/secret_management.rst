.. _secret_management:

Secret Management
=========================

The PostgreSQL instance deployed by the ``ska-db-oda-umbrella`` has authentication using a username and password.
The ``ska-db-oda`` REST server then uses this password to create a connection to the PostgreSQL instance.

The ``ska-db-oda-umbrella`` defines a template to create a Kubernetes Secret that is pulled from Vault.
See the `Dev Portal <https://developer.skao.int/en/latest/reference/vault.html>`_ for more details.
This template is defined in the umbrella chart rather than the ``ska-db-oda`` REST server chart as it is primarily used by the PostgreSQL server.

To summarise, the Kubernetes Secret will be used in 3 places:

#. By the PostgreSQL chart as the password for instance it deploys.
#. By the ``ska-db-oda`` REST server to connect to the PostgreSQL instance.
#. By the pgadmin instance as its own password.

Other applications accessing the ODA
--------------------------------------

Within OSO, other applications such as ``ska-oso-services`` will also need the secret to connect to the ODA.

These application can follow the same process the ``ska-db-oda`` REST server does: make the Secret that is deployed alongside the ODA
available as environment variables using

.. code-block:: yaml

    env:
      - name: ADMIN_POSTGRES_PASSWORD
        valueFrom:
          secretKeyRef:
            name: ska-db-oda-umbrella-postgres-{{ $.Release.Name }}
            key: ADMIN_POSTGRES_PASSWORD
