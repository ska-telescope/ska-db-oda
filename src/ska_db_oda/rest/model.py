"""
This module contains the models that are used within the `rest` module that are used by the FastAPI app but are not part of the PDM. For example, query parameters or API error responses.
"""
from datetime import datetime
from enum import Enum
from typing import Optional

from pydantic import BaseModel, ConfigDict, Field
from ska_oso_pdm import OSOExecutionBlock, Project, Proposal, SBDefinition, SBInstance


class AppModel(BaseModel):
    """Base class for application data models - as distinct from PDM objects"""

    model_config = ConfigDict(
        extra="forbid", validate_default=True, validate_assignment=True
    )


class ErrorResponseDetail(AppModel):
    title: str
    description: str
    traceback: Optional[str] = None


class MatchType(Enum):
    EQUALS = "equals"
    STARTS_WITH = "starts_with"
    CONTAINS = "contains"


class DateQueryType(Enum):
    CREATED_BETWEEN = "created_between"
    MODIFIED_BETWEEN = "modified_between"


class ApiQueryParameters(AppModel):
    user: Optional[str] = None
    match_type: MatchType = MatchType.EQUALS
    query_type: Optional[DateQueryType] = None
    created_before: Optional[datetime] = None
    created_after: Optional[datetime] = None
    last_modified_before: Optional[datetime] = None
    last_modified_after: Optional[datetime] = None


class ApiStatusQueryParameters(AppModel):
    entity_id: str
    version: Optional[int] = None


class MultipleEntities(AppModel):
    """
    This is the request and response body for the PUT endpoint which is used to upload multiple entities at once.
    """

    sbds: list[SBDefinition] = Field(default_factory=list)
    sbis: list[SBInstance] = Field(default_factory=list)
    ebs: list[OSOExecutionBlock] = Field(default_factory=list)
    prjs: list[Project] = Field(default_factory=list)
    prsls: list[Proposal] = Field(default_factory=list)
