"""
This packages contains the functions that requests to each API resource are routed to. The REST resources are split into
individual modules.
"""
import json
from typing import Union

from ska_db_oda.persistence.domain import get_identifier, set_identifier
from ska_db_oda.persistence.domain.query import QueryParams, QueryParamsFactory
from ska_db_oda.rest.errors import UnprocessableEntityError
from ska_db_oda.rest.model import ApiQueryParameters, ApiStatusQueryParameters


def check_for_mismatch(path_id: str, entity_id: str) -> None:
    if path_id != entity_id:
        raise UnprocessableEntityError(
            detail=(
                "There is a mismatch between the identifier for the endpoint"
                f" {path_id} and the JSON payload {entity_id}"
            )
        )


def identifier_check(entity) -> None:
    """
    When sending a POST request to create a new entity, the new entity will get a new identifier
    fetched from SKUID in the repository layer. We raise an error if the user sends a POST request with an identifier
    in the body, to make it clear to the user the entity will not be created with this identifier.
    """

    if get_identifier(entity):
        set_identifier(entity, None)
        # TODO this error should be raised but this was too confusing of a change to make in the same MR as converting to FastAPI
        # raise BadRequestError(
        #     detail=(
        #         "Entity ID given in the body of the POST request. Identifier"
        #         " generation for new entities is the responsibility of the ODA,"
        #         " which will fetch them from SKUID, so they should not be given in"
        #         " this request."
        #     ))


def get_qry_params(
    api_query_parameters: Union[ApiQueryParameters, ApiStatusQueryParameters]
) -> QueryParams:
    """
    Convert the parameters from the request into the correct extension of QueryParams.

    Currently only a single instance of QueryParams is supported, so
    subsequent parameters will be ignored.

    :param api_query_parameters: DeserializedHTTP query parameters deserialized
    :return: An instance of QueryParams
    :raises: UnprocessableEntityError if a supported QueryParams cannot be extracted
    """
    try:
        # This is a hangover from before we had FastAPI/Pydantic models for the API inputs. Instead we had
        # repository layer models
        # This isn't the nicest bit of code, and will be refactored once we extend the querying functionality.
        return QueryParamsFactory.from_dict(
            json.loads(api_query_parameters.model_dump_json())
        )
    except ValueError as err:
        raise UnprocessableEntityError(detail=err.args[0]) from err
