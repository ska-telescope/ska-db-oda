import logging
from typing import Union

from fastapi import APIRouter
from ska_oso_pdm import OSOExecutionBlock, Project, Proposal, SBDefinition, SBInstance

from ska_db_oda.persistence import oda
from ska_db_oda.rest.errors import UnprocessableEntityError
from ska_db_oda.rest.model import MultipleEntities

LOGGER = logging.getLogger(__name__)

base_router = APIRouter()

OSOPrimaryEntity = Union[OSOExecutionBlock, Project, Proposal, SBDefinition, SBInstance]


@base_router.put(
    "/",
    summary="Save multiple entities",
    description="Used to upload multiple entities in a single atomic operation.",
    response_model=MultipleEntities,
)
def put_entities(entities: MultipleEntities) -> MultipleEntities:
    """
    Store the uploaded SBDs in the ODA.

    Successful storage depends on:
      - a correctly formed request
      - SBDs are unique, valid, and can be deserialised correctly

    A failure response will be returned if the request does not meet the
    conditions above.

    :return: HTML response
    """

    contains_duplicate_entities(entities)

    return commit_entities(entities)


def contains_duplicate_entities(entities: MultipleEntities) -> None:
    """
    Validate a JSON request for duplicate entity IDs.

    This validator checks whether the entities in the JSON request are unique. If
    not, an error response is returned.

    :param entities: request to validate
    :raises: ValueError if all entity IDs are not unique
    """
    key_to_id = {
        "sbds": "sbd_id",
        "sbis": "sbi_id",
        "ebs": "eb_id",
        "prjs": "prj_id",
        "prsls": "prsl_id",
    }
    duplicate_ids = []
    for key, id_field in key_to_id.items():
        if not getattr(entities, key):
            continue
        entity_ids = [
            getattr(entity, id_field)
            for entity in getattr(entities, key)
            if getattr(entity, id_field) is not None
        ]
        if len(set(entity_ids)) != len(entity_ids):
            duplicate_ids.append(entity_ids)

    if duplicate_ids:
        raise UnprocessableEntityError(
            detail=(
                "Unprocessable Entity, duplicate IDs in payload. Entity ID sets with"
                f" duplicates: {duplicate_ids}"
            )
        )


def commit_entities(entities: MultipleEntities) -> MultipleEntities:
    with oda.uow() as uow:
        for sbd in entities.sbds:
            uow.sbds.add(sbd)
        for sbi in entities.sbis:
            uow.sbis.add(sbi)
        for eb in entities.ebs:
            uow.ebs.add(eb)
        for prj in entities.prjs:
            uow.prjs.add(prj)
        for prsl in entities.prsls:
            uow.prsls.add(prsl)
        uow.commit()

    return entities
