from abc import abstractmethod
from copy import deepcopy
from datetime import datetime, timezone
from typing import Generic, Optional, TypeVar

from ska_oso_pdm import Metadata

T = TypeVar("T")


class MetadataMixin(Generic[T]):
    """
    A mixin class which provides common functionality for updating the metadata of ODA entities.

    It is designed to be mixed in with a Repository implementation, and should never be
    directly instantiated
    """

    @abstractmethod
    def _get_latest_metadata(self, entity: T) -> Optional[Metadata]:
        """Retrieves the Metadata for the latest version of the entity in the persistent storage.

        It is abstract as the implementation depends on how the entities are stored, eg in postgres.

        :param entity: An OSO entity which contains Metadata
        :return: Metadata for the latest version of the entity, or None if the a version of the entity
                doesn't exist yet
        :rtype: Optional[MetaData]
        """
        raise NotImplementedError

    def update_metadata(
        self,
        entity: T,
        last_modified_by: Optional[str] = None,
    ) -> T:
        """Updates the metadata of a copy of the entity

        If a version of the entity already exists in the ODA, the previous version will be incremented and the last modified fields updated.

        If a version does not already exist, a new metadata block will be created.

        :param entity: An ODA entity submitted to be persisted
        :type entity: An ODA entity which contains Metadata
        :param last_modified_by: The user performing the operation
        :type last_modified_by: str
        :return: A copy of the entity with the updated metadata to be persisted
        :rtype: An ODA entity which contains Metadata
        """
        # This is a temporary measure to indicate that the last_modified_by field will be stored in the database,
        # but currently it is uncertain which layer of the architecture this will come from, and
        # eg whether is will be set in the entity json or extracted from HTTP headers

        if last_modified_by is None:
            last_modified_by = "DefaultUser"

        latest_metadata = self._get_latest_metadata(entity)

        if latest_metadata:
            updated_entity = deepcopy(entity)
            updated_entity.metadata.version = latest_metadata.version + 1

            updated_entity.metadata.last_modified_on = datetime.now(tz=timezone.utc)
            updated_entity.metadata.last_modified_by = last_modified_by
            # These fields should be the same, but this protects against potential overwrites of
            # metadata that should be immutable across all versions of an entity
            updated_entity.metadata.created_on = latest_metadata.created_on
            updated_entity.metadata.created_by = latest_metadata.created_by

        else:
            updated_entity = self._set_new_metadata(entity, last_modified_by)

        return updated_entity

    def _set_new_metadata(self, entity: T, created_by: Optional[str] = None) -> T:
        """
        Set the metadata for a new entity, with version 1, created_on and last_modified_on set to the current time
        and created_on and last_modified_by both set to the same value

        :param entity: An ODA entity submitted to be persisted
        :type entity: An ODA entity which contains Metadata
        :param created_by: The user performing the operation
        :type created_by: str
        :return: A copy of the entity with the new metadata to be persisted
        """
        # This is a temporary measure to indicate that the last_modified_by field will be stored in the database,
        # but currently it is uncertain which layer of the architecture this will come from, and
        # eg whether is will be set in the entity json or extracted from HTTP headers
        if created_by is None:
            created_by = "DefaultUser"

        entity = deepcopy(entity)
        now = datetime.now(tz=timezone.utc)

        entity.metadata = Metadata(
            version=1,
            created_on=now,
            created_by=created_by,
            last_modified_on=now,
            last_modified_by=created_by,
        )

        return entity
