"""
Custom errors that should be raised within the persistence package
"""
from typing import Optional


class ODAError(RuntimeError):
    """
    General exception raised by the ODA when other errors aren't applicable
    """

    def __init__(self, message: Optional[str] = None) -> None:
        if message:
            self.message = message


class StatusHistoryException(ODAError):
    """
    Exception raised when a status history exception occur
    """

    def __init__(self, message: str) -> None:
        super().__init__(message)


class EntityNotSupported(ODAError):
    """
    Exception raised when a method is passed an entity type it does not support
    """

    def __init__(self, entity: type) -> None:
        message = f"Entity type {entity.__class__.__name__} not supported"
        super().__init__(message)


class QueryParameterError(ODAError):
    """
    Exception raised when there is a problem with the query parameters, e.g. the combination is not allowed
    """

    def __init__(
        self, *, qry_params: Optional[type] = None, message: Optional[str] = None
    ) -> None:
        if message:
            pass
        elif qry_params:
            message = f"Unsupported query type {qry_params.__class__.__name__}"
        else:
            message = "Exception raised while handling query parameters"

        super().__init__(message)


class ODANotFound(ODAError):
    """
    Exception raised when an entity cannot be found in the data store
    """

    def __init__(
        self, *, identifier: Optional[str] = None, message: Optional[str] = None
    ) -> None:
        if message:
            pass
        elif identifier:
            message = f"The requested identifier {identifier} could not be found."
        else:
            message = "The requested identifier could not be found."

        super().__init__(message)
