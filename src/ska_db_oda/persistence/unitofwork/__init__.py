from os import getenv

from ska_db_oda.persistence.unitofwork.filesystemunitofwork import FilesystemUnitOfWork
from ska_db_oda.persistence.unitofwork.postgresunitofwork import (
    PostgresUnitOfWork,
    create_connection_pool,
)

ODA_BACKEND_TYPE = getenv("ODA_BACKEND_TYPE", "filesystem")
