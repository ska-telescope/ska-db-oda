"""
This module contains the UnitOfWork Abstract Base Class.

All UnitOfWork implementations to conform to this interface.
"""

import logging
from abc import ABC, abstractmethod

from ska_db_oda.persistence.domain.repository import (
    EntityRelationshipRepository,
    ExecutionBlockRepository,
    ExecutionBlockStatusHistoryRepository,
    ProjectRepository,
    ProjectStatusHistoryRepository,
    ProposalRepository,
    SBDefinitionRepository,
    SBDefinitionStatusHistoryRepository,
    SBInstanceRepository,
    SBInstanceStatusHistoryRepository,
)

LOGGER = logging.getLogger(__name__)


class AbstractUnitOfWork(ABC):
    """Provides the interface to store or retrieve a group of OSO Entities.

    Commits or rollsback a series of database transactions as an atomic operation
    """

    sbds: SBDefinitionRepository
    sbis: SBInstanceRepository
    ebs: ExecutionBlockRepository
    prjs: ProjectRepository
    prsls: ProposalRepository
    entity_relationship: EntityRelationshipRepository
    sbds_status_history: SBDefinitionStatusHistoryRepository
    sbis_status_history: SBInstanceStatusHistoryRepository
    ebs_status_history: ExecutionBlockStatusHistoryRepository
    prjs_status_history: ProjectStatusHistoryRepository

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if (exc_type, exc_value, traceback) == (None, None, None):
            LOGGER.debug(
                "UnitOfWork exited successfully. Rolling back any uncommitted"
                " transactions."
            )
        else:
            LOGGER.error(
                (
                    "UnitOfWork exited with error '%s : %s'. Rolling back any"
                    " uncommitted transactions."
                ),
                exc_type,
                exc_value,
            )
        self.rollback()

    @abstractmethod
    def commit(self) -> None:
        """Commits the entities added to this Unit of Work to the database, as an atomic transaction.

        :raises ValueError: if the validation of committed sbds or the metadata fails
        """
        raise NotImplementedError

    @abstractmethod
    def rollback(self) -> None:
        """Initiates the rollback of this Unit of Work, so that none of the entities are persisted.

        If no commit is carried out or an error is raised,
        the unit of work is rolled back to a safe state
        """
        raise NotImplementedError
