"""
This module contains implementations of the AbstractRepository class, using
the filesystem as the data store.
"""
import functools
import json
import logging
import operator
import os
import re
from os import W_OK, PathLike, access, environ
from pathlib import Path
from typing import Dict, List, Optional, TypeVar, Union

from ska_oso_pdm import Metadata
from ska_oso_pdm.entity_status_history import (
    OSOEBStatusHistory,
    ProjectStatusHistory,
    SBDStatusHistory,
    SBIStatusHistory,
)

from ska_db_oda.persistence.domain import (
    OSOEntity,
    get_identifier,
    get_identifier_or_fetch_from_skuid,
    is_entity_status_history,
)
from ska_db_oda.persistence.domain.errors import (
    ODAError,
    ODANotFound,
    QueryParameterError,
)
from ska_db_oda.persistence.domain.query import (
    DateQuery,
    MatchType,
    QueryParams,
    StatusQuery,
    UserQuery,
)
from ska_db_oda.persistence.domain.repository import RepositoryBridge
from ska_db_oda.persistence.infrastructure.filesystem.mapping import (
    EntityRelationshipMapping,
    FilesystemMapping,
    get_status_entity_version,
)

LOGGER = logging.getLogger(__name__)

T = TypeVar("T", bound=OSOEntity)
U = TypeVar("U")


class FilesystemBridge(RepositoryBridge[T, U]):
    """
    Implementation of the Repository bridge which persists entities to a filesystem.

    Entities will be stored under the following filesystem structure:
    `/<base_working_dir>/<entity_type_dir/<entity_id>/<version>.json`
    For example, by default version 1 of an SBDefinition with sbd_id sbi-mvp01-20200325-00001
    will be stored at: `/var/lib/oda/sbd/sbi-mvp01-20200325-00001/1.json`
    """

    def __init__(
        self,
        filesystem_mapping: FilesystemMapping,
        base_working_dir: Union[str, PathLike] = Path("/var/lib/oda"),
    ):
        base_working_dir = Path(environ.get("ODA_DATA_DIR", base_working_dir))
        LOGGER.info(
            "Initializing ODA filesystem backend. Working directory=%s",
            base_working_dir,
        )
        if not base_working_dir.is_dir():
            raise ODAError(message=f"Directory {base_working_dir} not found")
        if not access(base_working_dir, W_OK):
            raise ODAError(message=f"Directory {base_working_dir} not writable")

        self._base_working_dir = base_working_dir
        self.working_dir = self._base_working_dir / filesystem_mapping.entity_type_dir
        Path(self.working_dir).mkdir(parents=True, exist_ok=True)

        self._transactions: Dict[Path, str] = {}
        if not isinstance(filesystem_mapping, EntityRelationshipMapping):
            self._serialise = filesystem_mapping.serialise
            self._deserialise = filesystem_mapping.deserialise
            self._entity_id_from_path = filesystem_mapping.entity_id_from_path
        self._filesystem_mapping = filesystem_mapping

    def __len__(self):
        """
        Return the size of this repository.

        Note that this is a naive implementation that simply counts the number
        JSON files in the working directory. It does not verify that each JSON
        file is a serialized, valid SB.
        """
        return sum(1 for _ in self.working_dir.rglob("*.json"))

    def __contains__(self, entity_id: U):
        """
        Return True if a version of an entity with the given ID is present in this repository.

        :param entity_id: ID to search for
        """
        entity_dir = self._path_for_entity_id_dir(entity_id)
        return bool(entity_dir.exists() and os.listdir(entity_dir))

    def create(self, entity: T) -> T:
        """Implementation of the RepositoryBridge method.

        To mimic the real database, entities are added to a list of pending transactions and only
        written to the filesystem when the unit of work is committed.

        See :func:`~ska_db_oda.persistence.domain.repository.RepositoryBridge.create` docstring for details
        """
        entity_id = get_identifier_or_fetch_from_skuid(entity)

        entity = self.update_metadata(entity)

        Path(self._path_for_entity_id_dir(entity_id)).mkdir(parents=True, exist_ok=True)

        entity_path = self._path_for_entity(entity)
        LOGGER.debug(
            "Adding entity with ID %s to the filesystem transactions under path %s",
            entity_id,
            entity_path,
        )

        if isinstance(
            entity,
            (
                SBDStatusHistory,
                SBIStatusHistory,
                OSOEBStatusHistory,
                ProjectStatusHistory,
            ),
        ):
            self._transactions[entity_path] = self._serialise(
                entity=entity, path=entity_path
            )

        else:
            self._transactions[entity_path] = self._serialise(entity=entity)

        return entity

    def read(
        self,
        entity_id: U,
        version: U = None,
        is_status_history: bool = False,
    ) -> T:
        """
        Gets the latest version of the entity with the given entity_id.

        As this method will always be accessed in the context of a UnitOfWork, the pending transactions
        also need to be checked for a version to return.
        (Similar to with a database implementation where an entity that was added to a transaction but
        not committed would still be accessible inside the transaction.)
        """

        LOGGER.debug("Getting entity with ID %s from the filesystem", entity_id)
        pending_versions = [
            int(os.path.splitext(path.name)[0])
            for path in self._transactions.keys()
            if entity_id in str(path)
        ]

        entity_dir_path = self._path_for_entity_id_dir(entity_id)
        # The metadata checks will mean a version in the pending transactions would always
        # be a newer version than any in the filesystem, so check the pending versions first.
        if pending_versions:
            latest_entity_path = entity_dir_path / f"{max(pending_versions)}.json"
            return self._deserialise(self._transactions[latest_entity_path])

        if entity_dir_path.exists() and (
            versions := [
                int(os.path.splitext(entity_path.name)[0])
                for entity_path in entity_dir_path.glob("[0-9]*.json")
            ]
        ):
            version_to_query = version if version else max(versions)
            if is_status_history:
                latest_entity_path = (
                    entity_dir_path / f"status_history_{version_to_query}.json"
                )

                if latest_entity_path.exists():
                    return self._deserialise(
                        json_string=latest_entity_path.read_text(),
                        history=is_status_history,
                    )
                raise ODANotFound(message=f"No status entity for {entity_id}")

            else:
                latest_entity_path = entity_dir_path / f"{version_to_query}.json"
                return self._deserialise(json_string=latest_entity_path.read_text())

        raise ODANotFound(identifier=entity_id)

    def read_relationship(
        self, entity_id: U, parent_entity: U, associated_entity: U
    ) -> T:
        """
        Gets the latest version of the associated entity with the given parent entity entity_id.
        This is used for join relationships.
        :param entity_id: entity id for search.
        :param parent_entity: primary entity for join relationship.
        :param associated_entity: secondary entity for join relationship.
        Return list of entity data based on relationship.
        """
        LOGGER.debug("Getting entity with ID %s from the filesystem", entity_id)

        parent_entity_file_obj = self._filesystem_mapping.get_mapping(parent_entity)
        associated_entity_file_obj = self._filesystem_mapping.get_mapping(
            associated_entity
        )
        if parent_entity_file_obj is None or associated_entity_file_obj is None:
            raise ODANotFound(
                message=(
                    f"The requested associated entity for given {entity_id} could not"
                    " be found."
                )
            )
        base_working_dir = (
            self._base_working_dir / parent_entity_file_obj.entity_type_dir
        )
        parent_entity_dir_path = self._path_for_parent_file_id_dir(
            base_working_dir, entity_id
        )

        # The metadata checks will mean a version in the pending transactions would always
        # be a newer version than any in the filesystem, so check the pending versions first.

        file_content = self._read_entity_data_using_file_path(
            entity_id, parent_entity_dir_path
        )
        deserialise_entity = parent_entity_file_obj.deserialise(file_content)
        return self._get_join_records_from_entities(
            deserialise_entity,
            parent_entity_file_obj,
            associated_entity_file_obj,
        )

    def _get_join_records_from_entities(
        self,
        deserialise_entity: str,
        parent_entity_file_obj,
        associated_entity_file_obj,
    ) -> list:
        """
        Match data between two files based on entity id and relational file.
        :param deserialise_entity: stored entity data into file.
        :param parent_entity_file_obj: parent entity file object to access serialise, deserialise entity.
        :param associated_entity_file_obj: associated entity file object to access serialise, deserialise entity.
        :Return list of entities data based on given entity id and relational table.
        e.g: - ebs/123/sbi from given URL it will return SBI associated with an EB #123

        """
        entity_id = json.loads(parent_entity_file_obj.serialise(deserialise_entity))[
            associated_entity_file_obj.relationship_identifier
        ]
        entity_dir_path = (
            self._base_working_dir
            / associated_entity_file_obj.entity_type_dir
            / entity_id
        )
        joined_table_data = self._read_entity_data_using_file_path(
            entity_id, entity_dir_path
        )
        return associated_entity_file_obj.deserialise(joined_table_data)

    def _read_entity_data_using_file_path(
        self, entity_id: str, entity_dir_path: str
    ) -> str:
        """
        Read json file based on entity dir path and entity id
        :param entity_id: provided entity id from parameter.
        :entity_dir_path: dir path to read json file.
        Return file data based on entity_id and entity dir path.
        """
        # Filenames are of the form 1.json
        if entity_dir_path.exists():
            versions = [
                int(os.path.splitext(entity_path.name)[0])
                for entity_path in entity_dir_path.glob("[0-9]*.json")
            ]
            if versions:
                latest_entity_path = entity_dir_path / f"{max(versions)}.json"

                return latest_entity_path.read_text()
        raise ODANotFound(identifier=entity_id)

    def update(self, entity: T) -> T:
        """Implementation of the RepositoryBridge method.

        To mimic the real database, entities are added to a list of pending transactions and only
        written to the filesystem when the unit of work is committed.

        See :func:`~ska_db_oda.persistence.domain.repository.RepositoryBridge.update` docstring for details
        """
        entity_id = get_identifier_or_fetch_from_skuid(entity)
        entity = self._set_new_metadata(entity)

        Path(self._path_for_entity_id_dir(entity_id)).mkdir(parents=True, exist_ok=True)

        entity_path = self._path_for_entity(entity)
        LOGGER.debug(
            "Adding entity with ID %s to the filesystem transactions under path %s",
            entity_id,
            entity_path,
        )
        serialised_entity = self._serialise(entity)
        self._transactions[entity_path] = serialised_entity
        return entity

    def query(
        self, qry_params: QueryParams, is_status_history: Optional[bool] = None
    ) -> List[T]:
        """
        :param is_status_history: True if the query is for status history else False
        """

        # strategy for this implementation is to:
        #
        # 1. create a list of filter functions matching the requirements of the query
        # 2. for each entity in the repo, apply the filter functions
        # 3. if the entity passes each test, add its ID to the list of results
        #
        # With this strategy we can reuse filter functions to build compound
        # complex queries, e.g., entities created by user X after 1/1/2023
        if isinstance(qry_params, StatusQuery) and is_status_history:
            entity = self.read(
                qry_params.entity_id,
                qry_params.version,
                is_status_history=is_status_history,
            )

            return entity
        else:
            final_result = []
            filter_fns = QueryFilterFactory.filter_functions_for_query(qry_params)
            for entity_id in self._all_entity_ids():
                entity = self.read(entity_id)
                if all(fn(entity) for fn in filter_fns):
                    final_result.append(entity)
            return final_result

    def _get_latest_metadata(self, entity: T) -> Optional[Metadata]:
        """Implementation of the abstract MetaDataMixin method for a filesystem backend.

        See :func:`~ska_db_oda.persistence.domain.metadatamixin.MetadataMixin._get_latest_metadata` docstring for details
        """
        try:
            is_status_history = is_entity_status_history(entity)
            read_result = self.read(
                get_identifier(entity), is_status_history=is_status_history
            )
            return (
                read_result[0].metadata if is_status_history else read_result.metadata
            )
        except ODANotFound:
            return None

    def _path_for_entity(self, entity: T) -> Path:
        """
        Returns the final part of path where the serialised entity is stored,
        eg `sbd/sbi-mvp01-20200325-00001/2.json`
        """

        if isinstance(
            entity,
            (
                SBDStatusHistory,
                SBIStatusHistory,
                OSOEBStatusHistory,
                ProjectStatusHistory,
            ),
        ):
            version = get_status_entity_version(entity)

            return (
                self.working_dir
                / get_identifier(entity)
                / f"status_history_{version}.json"  # pylint: disable=W0123
            )
        return (
            self.working_dir
            / get_identifier(entity)
            / f"{entity.metadata.version}.json"
        )

    def _path_for_entity_id_dir(self, entity_id: U) -> Path:
        """
        Returns the path of the directory that all versions of the entity with the given
        entity_id are stored under, eg `/var/lib/oda/sbd/sbi-mvp01-20200325-00001/`
        """
        return self.working_dir / str(entity_id)

    def _path_for_parent_file_id_dir(self, working_dir: U, entity_id: U) -> Path:
        """
        Returns the path of the directory that all versions of the entity with the given
        entity_id are stored under, eg `/var/lib/oda/sbd/sbi-mvp01-20200325-00001/`
        """
        return working_dir / str(entity_id)

    def _all_entity_ids(self) -> List[U]:
        """
        Return a list of entity IDs, one entity ID for each entity stored in
        the repository.
        """
        # path format is <entity ID>/<version>.json so the list of directories
        # in the working directory should give a list of entity IDs.
        # iterdir() gives paths whereas we entity IDs of the correct type,
        # hence we call _entity_id_from_path for each directory
        return [
            self._entity_id_from_path(f)
            for f in self.working_dir.iterdir()
            if f.is_dir()
        ]


class QueryFilterFactory:
    """
    Factory class that returns a list of Python functions equivalent to a user
    query. Each function processes an entity, returning True if the entity
    passes the query test.
    """

    @staticmethod
    def filter_functions_for_query(query: QueryParams):
        filter_fns = []
        if isinstance(query, UserQuery):
            filter_fns.append(QueryFilterFactory.match_editor(query))
        elif isinstance(query, DateQuery):
            filter_fns.append(QueryFilterFactory.filter_between_dates(query))
        elif isinstance(query, StatusQuery):
            filter_fns.append(QueryFilterFactory.status_match_editor(query))
        else:
            raise QueryParameterError(qry_params=query)
        return filter_fns

    @staticmethod
    def match_editor(query: UserQuery):
        """
        Returns a function that returns True if a document editor matches a
        (sub)string.
        """

        def regex_match(obj):
            """
            created new function for pattern match
            """
            pattern = re.compile(r"{}".format(query.user))
            if pattern.search(obj):
                return obj

        if query.match_type == MatchType.EQUALS:
            match_fn = functools.partial(operator.eq, query.user)
        elif query.match_type == MatchType.STARTS_WITH:
            match_fn = operator.methodcaller("startswith", query.user)
        elif query.match_type == MatchType.CONTAINS:
            # created custom function for regex and pass to match_fn
            match_fn = regex_match

        else:
            raise QueryParameterError(message=f"Invalid match type: {query.match_type}")

        def match(obj):
            return match_fn(obj.metadata.created_by) or match_fn(
                obj.metadata.last_modified_by
            )

        return match

    @staticmethod
    def status_match_editor(query: StatusQuery):
        """
        Returns a function that returns True if a document editor matches a
        (sub)string.
        """

        def regex_match(entity):
            """
            created new function for pattern match
            """
            pattern = re.compile(r"{}".format(query.entity_id))
            if pattern.search(entity):
                return entity

        def match(entity):
            return regex_match(get_identifier(entity))

        return match

    @staticmethod
    def filter_between_dates(query: DateQuery):
        """
        Returns a function that returns True if a date is between a given range.
        """

        if query.query_type == DateQuery.QueryType.CREATED_BETWEEN:
            accessor = operator.attrgetter("metadata.created_on")
        elif query.query_type == DateQuery.QueryType.MODIFIED_BETWEEN:
            accessor = operator.attrgetter("metadata.last_modified_on")
        else:
            raise QueryParameterError(
                message=f"Unrecognized date query type: {query.query_type}"
            )

        if query.start is None and query.end is None:
            raise QueryParameterError(
                message="Query start and query end can not be None"
            )

        if (
            query.start is not None
            and query.end is not None
            and query.start >= query.end
        ):
            raise QueryParameterError(
                message="Query end date must be later than query start date"
            )

        def ge_start(value):
            # always match if no start date specified
            if query.start is None:
                return True
            return value.timestamp() >= query.start.timestamp()

        def lt_end(value):
            # always match if no end date specified
            if query.end is None:
                return True
            return value.timestamp() < query.end.timestamp()

        def match(obj):
            o = accessor(obj)
            return ge_start(o) and lt_end(o)

        return match
