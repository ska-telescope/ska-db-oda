import json
import os
from abc import ABC, abstractmethod
from pathlib import Path
from typing import List, TypeVar

from ska_oso_pdm import OSOExecutionBlock, Project, Proposal, SBDefinition, SBInstance
from ska_oso_pdm.entity_status_history import (
    OSOEBStatusHistory,
    ProjectStatusHistory,
    SBDStatusHistory,
    SBIStatusHistory,
)
from ska_oso_pdm.sb_definition import SBDefinitionID

from ska_db_oda.constant import EBS, ENTITY_CONSTANTS, PRJ, PRSLS, SBDS, SBIS
from ska_db_oda.persistence.domain import OSOEntity

T = TypeVar("T", bound=OSOEntity)
U = TypeVar("U")


class FilesystemMapping:
    """
    A class which provides functionality for mapping between entities stored in the ODA
    and the filesystem location they are persisted.
    """

    @property
    @abstractmethod
    def entity_type_dir(self) -> Path:
        """
        The directory under the base working directory that the entity type is stored under.

        For example, by default SBDefinitions will be stored under '/var/lib/oda/sbd', with
        'var/lib/oda' being the base working directory and 'sbd' being the value of this property.
        """
        raise NotImplementedError

    @property
    @abstractmethod
    def relationship_identifier(self) -> Path:
        """
        Search key indentifier to find entity data from the file.
        This is used to join relationship files.
        """
        raise NotImplementedError

    @abstractmethod
    def serialise(self, entity: T) -> str:
        raise NotImplementedError

    @abstractmethod
    def deserialise(self, json_string: str) -> T:
        raise NotImplementedError

    @abstractmethod
    def entity_id_from_path(self, path: Path) -> U:
        """
        Convert an entity storage path to an entity ID of the correct type.

        This is required as one day the ID types might be complex classes rather
        than primitive strings, and we'd need to convert them to the appropriate
        type.
        """
        raise NotImplementedError


class EntityRelationshipFileSystemMapping(ABC):
    """
    Base abstract class for managing relationships between different entities.
    """

    @property
    @abstractmethod
    def entity_type_dir(self) -> Path:
        """
        The directory under the base working directory that the entity type is stored under.

        For example, by default SBDefinitions will be stored under '/var/lib/oda/sbd', with
        'var/lib/oda' being the base working directory and 'sbd' being the value of this property.
        """
        raise NotImplementedError

    def get_mapping(self, entity_type: str) -> str:
        """
        Returns the mapping instance for the given entity type.
        """
        raise NotImplementedError


class SBDefinitionMapping(FilesystemMapping):
    @property
    def entity_type_dir(self):
        return Path("sbd")

    @property
    def relationship_identifier(self):
        return ENTITY_CONSTANTS[SBDS]

    def serialise(self, entity: SBDefinition) -> str:
        return entity.model_dump_json()

    def deserialise(self, json_string: str) -> SBDefinition:
        return SBDefinition.model_validate_json(json_string)

    def entity_id_from_path(self, path: Path) -> SBDefinitionID:
        return SBDefinitionID(path.stem)


class ExecutionBlockMapping(FilesystemMapping):
    @property
    def entity_type_dir(self):
        return Path("eb")

    @property
    def relationship_identifier(self):
        return ENTITY_CONSTANTS[EBS]

    def serialise(self, entity: OSOExecutionBlock) -> str:
        return entity.model_dump_json()

    def deserialise(self, json_string: str) -> OSOExecutionBlock:
        return OSOExecutionBlock.model_validate_json(json_string)

    def entity_id_from_path(self, path: Path) -> str:
        return str(path.stem)


class SBInstanceMapping(FilesystemMapping):
    @property
    def entity_type_dir(self):
        return Path("sbi")

    @property
    def relationship_identifier(self):
        return ENTITY_CONSTANTS[SBIS]

    def serialise(self, entity: SBInstance) -> str:
        return entity.model_dump_json()

    def deserialise(self, json_string: str) -> SBInstance:
        return SBInstance.model_validate_json(json_string)

    def entity_id_from_path(self, path: Path) -> str:
        return str(path.stem)


class ProjectMapping(FilesystemMapping):
    @property
    def entity_type_dir(self):
        return Path("prj")

    @property
    def relationship_identifier(self):
        return ENTITY_CONSTANTS[PRJ]

    def serialise(self, entity: Project) -> str:
        return entity.model_dump_json()

    def deserialise(self, json_string: str) -> Project:
        return Project.model_validate_json(json_string)

    def entity_id_from_path(self, path: Path) -> str:
        return str(path.stem)


class ProposalMapping(FilesystemMapping):
    @property
    def entity_type_dir(self):
        return Path("prsl")

    @property
    def relationship_identifier(self):
        return ENTITY_CONSTANTS[PRSLS]

    def serialise(self, entity: Proposal) -> str:
        return entity.model_dump_json()

    def deserialise(self, json_string: str) -> Proposal:
        return Proposal.model_validate_json(json_string)

    def entity_id_from_path(self, path: Path) -> str:
        return str(path.stem)


class EntityRelationshipMapping(EntityRelationshipFileSystemMapping):
    def __init__(self) -> None:
        """
        Initial entity mapping for entities.
        """
        self.sbds = SBDefinitionMapping()
        self.sbis = SBInstanceMapping()
        self.ebs = ExecutionBlockMapping()

    @property
    def entity_type_dir(self):
        return Path("")

    def get_mapping(self, entity_type: str) -> T:
        """
        Returns the mapping instance for the given entity type.
        """
        entities_file_mapping = {EBS: self.ebs, SBIS: self.sbis, SBDS: self.sbds}
        return entities_file_mapping.get(entity_type)


class SBDefinitionStatusHistoryMapping(FilesystemMapping):
    @property
    def entity_type_dir(self):
        return Path("sbd")

    @property
    def relationship_identifier(self):
        return ENTITY_CONSTANTS[SBDS]

    def serialise(  # pylint: disable=W0222
        self, entity: SBDStatusHistory, path: Path = None
    ) -> str:
        return common_serialise_for_status_entity(entity, path)

    def deserialise(
        self, json_string: str, history: bool = False
    ) -> List[SBDStatusHistory]:
        return common_deserialise_for_status_entity(
            history, json_string, SBDStatusHistory
        )

    def entity_id_from_path(self, path: Path) -> str:
        return str(path.stem)


class OSOExecutionBlockStatusHistoryMapping(FilesystemMapping):
    @property
    def entity_type_dir(self):
        return Path("eb")

    @property
    def relationship_identifier(self):
        return ENTITY_CONSTANTS[EBS]

    def serialise(  # pylint: disable=W0222
        self, entity: OSOEBStatusHistory, path: Path = None
    ) -> str:
        return common_serialise_for_status_entity(entity, path)

    def deserialise(
        self, json_string: str, history: bool = False
    ) -> List[OSOEBStatusHistory]:
        return common_deserialise_for_status_entity(
            history, json_string, OSOEBStatusHistory
        )

    def entity_id_from_path(self, path: Path) -> str:
        return str(path.stem)


class SBInstanceStatusHistoryMapping(FilesystemMapping):
    @property
    def entity_type_dir(self):
        return Path("sbi")

    @property
    def relationship_identifier(self):
        return ENTITY_CONSTANTS[SBIS]

    def serialise(  # pylint: disable=W0222
        self, entity: SBIStatusHistory, path: Path = None
    ) -> str:
        return common_serialise_for_status_entity(entity, path)

    def deserialise(
        self, json_string: str, history: bool = False
    ) -> List[SBIStatusHistory]:
        return common_deserialise_for_status_entity(
            history, json_string, SBIStatusHistory
        )

    def entity_id_from_path(self, path: Path) -> str:
        return str(path.stem)


class ProjectStatusHistoryMapping(FilesystemMapping):
    @property
    def entity_type_dir(self):
        return Path("prj")

    @property
    def relationship_identifier(self):
        pass

    def serialise(  # pylint: disable=W0222
        self, entity: ProjectStatusHistory, path: Path = None
    ) -> str:
        return common_serialise_for_status_entity(entity, path)

    def deserialise(
        self, json_string: str, history: bool = False
    ) -> List[ProjectStatusHistory]:
        return common_deserialise_for_status_entity(
            history, json_string, ProjectStatusHistory
        )

    def entity_id_from_path(self, path: Path) -> str:
        return str(path.stem)


def insert_json_file(data, path: Path) -> None:
    """This function writes status history data in json status
        file

    :param data: entity object
    :param path: json file path.
    :return: None
    """
    with open(path, "w", encoding="utf-8") as json_file:
        json.dump(data, json_file)


def insert_status_data(entity: OSOEntity, path: Path):
    """This function creates status history data if not present
        and calls insert_json_file function and returns the
        original status history entity

    :param entity: entity object
    :param path: json file path.
    :return: None
    """
    entity_dict = entity.model_dump(mode="json")

    status_list = []

    status_list.append(entity_dict)

    insert_json_file(status_list, path)

    return json.dumps(status_list)


def check_and_append_status(entity: OSOEntity, path: Path):
    """This function checks status history data present or not in
        json file and appends new status history data. and returns
        original status history entity or raises StatusHistoryException

    :param entity: entity object
    :param path: json file path.
    :return: status entity or raises StatusHistoryException
    """

    with open(path, "r", encoding="utf-8") as json_file:
        json_data = json.load(json_file)

        entity_dict = entity.model_dump(mode="json")

        json_data.append(entity_dict)

        return json_data


def common_serialise_for_status_entity(entity: OSOEntity, path: Path) -> str:
    """This common serialise function is used to checks status history data present
        or not in and call other functions.

    :param entity: entity object
    :param path: json file path.
    :return: status entity or raises StatusHistoryException
    """

    if path is not None and os.path.isfile(path):
        json_data = check_and_append_status(entity, path)

        insert_json_file(json_data, path)

        return json.dumps(json_data)

    return insert_status_data(entity, path)


def common_deserialise_for_status_entity(
    history: bool, json_string: str, entity: OSOEntity
):
    """This common deserialise function is used to get status history data.

    :param entity: entity object
    :param path: json file path.
    :return: status entity or raises StatusHistoryException
    """
    string_to_json = json.loads(json_string)

    if history:
        history_list = []

        for entities in string_to_json:
            entity_status = entity.model_validate_json(json.dumps(entities))
            history_list.append(entity_status)

        return history_list

    # Getting latest status from list
    entity_status = entity.model_validate_json(json.dumps(string_to_json[-1]))
    return entity_status


def get_status_entity_version(entity: OSOEntity) -> int:
    """This function returns version of entity.

    :param entity: entity object
    :return: version of entity
    """

    for key, value in vars(entity).items():
        if "version" in key:
            return value
