"""
Pure functions which map from entities to SQL queries with parameters
"""
from datetime import datetime
from typing import Callable, Dict, Tuple, Union

from psycopg import sql
from pydantic import BaseModel, Field
from ska_oso_pdm.sb_definition.sb_definition import Metadata

from ska_db_oda.persistence.domain import OSOEntity, get_identifier
from ska_db_oda.persistence.domain.errors import QueryParameterError
from ska_db_oda.persistence.domain.query import (
    DateQuery,
    MatchType,
    StatusQuery,
    UserQuery,
)

SqlTypes = Union[str, int, datetime]
QueryAndParameters = Tuple[sql.Composed, Tuple[SqlTypes]]


class TableDetails(BaseModel):
    table_name: str
    identifier_field: str
    # These dicts are keyed by the name of the table column and the value is a function which maps the entity to that column value
    column_map: dict[str, Callable[[OSOEntity], SqlTypes]]
    metadata_map: dict[str, Callable[[OSOEntity], SqlTypes]] = Field(
        default_factory=lambda: {
            "version": lambda entity: entity.metadata.version,
            "created_on": lambda entity: entity.metadata.created_on,
            "created_by": lambda entity: entity.metadata.created_by,
            "last_modified_on": lambda entity: entity.metadata.last_modified_on,
            "last_modified_by": lambda entity: entity.metadata.last_modified_by,
        }
    )

    def get_columns_with_metadata(self) -> Tuple[str]:
        return tuple(self.column_map.keys()) + tuple(
            self.metadata_map.keys()  # pylint: disable=no-member
        )

    # pylint: disable=no-member
    def get_params_with_metadata(self, entity: OSOEntity) -> Tuple[SqlTypes]:
        return tuple(map_fn(entity) for map_fn in self.column_map.values()) + tuple(
            map_fn(entity) for map_fn in self.metadata_map.values()
        )


def insert_query(table_details: TableDetails, entity: OSOEntity) -> QueryAndParameters:
    """
    Creates a query and parameters to insert the given entity in the table,
    effectively creating a new version by inserting a new row, and returning the row ID.

    :param table_details: The information about the table to perform the insert on.
    :param entity: The entity which will be persisted.
    :return: A tuple of the query and parameters, which psycopg will safely combine.
    """
    columns = table_details.get_columns_with_metadata()
    params = table_details.get_params_with_metadata(entity)
    query = sql.SQL(
        """
        INSERT INTO {table}
        ({fields})
        VALUES ({values})
        RETURNING id
        """
    ).format(
        table=sql.Identifier(table_details.table_name),
        fields=sql.SQL(",").join(map(sql.Identifier, columns)),
        values=sql.SQL(",").join(sql.Placeholder() * len(params)),
    )
    return query, params


def update_query(table_details: TableDetails, entity: OSOEntity) -> QueryAndParameters:
    """
    Creates a query and parameters to update the given entity in the table, overwriting values in the existing row and returning the row ID.

    If there is not an existing row for the identifier then no update is performed.

    :param table_details: The information about the table to perform the update on.
    :param entity: The entity which will be persisted.
    :return: A tuple of the query and parameters, which psycopg will safely combine.
    """
    columns = tuple(table_details.column_map.keys())
    params = tuple(map_fn(entity) for map_fn in table_details.column_map.values())

    query = sql.SQL(
        """
        UPDATE {table} SET ({fields}, "last_modified_by", "last_modified_on") = ({values}, %s, %s)
        WHERE id=(SELECT id FROM {table} WHERE {identifier_field}=%s AND "version"=1)
        RETURNING id;
        """
    ).format(
        identifier_field=sql.Identifier(table_details.identifier_field),
        table=sql.Identifier(table_details.table_name),
        fields=sql.SQL(",").join(map(sql.Identifier, columns)),
        values=sql.SQL(",").join(sql.Placeholder() * len(params)),
    )
    return query, params + (
        entity.metadata.last_modified_by,
        entity.metadata.last_modified_on,
        get_identifier(entity),
    )


def select_latest_query(
    table_details: TableDetails,
    entity_id: str,
    version: int = None,
) -> QueryAndParameters:
    """
    Creates a query and parameters to find the latest version of the given entity in the table, returning the row if found.

    :param table_details: The information about the table to perform the update on.
    :param entity_id: The identifier of the entity to search for.
    :return: A tuple of the query and parameters, which psycopg will safely combine.
    """
    columns = table_details.get_columns_with_metadata()

    order_by_clause_value = get_order_by_clause_value_for_status_entity(table_details)

    if version:
        where_clause = sql.SQL(
            "WHERE {identifier_field} = %s AND {order_by_clause} = %s ORDER BY id DESC"
            " LIMIT 1"
        ).format(
            identifier_field=sql.Identifier(table_details.identifier_field),
            order_by_clause=sql.Identifier(order_by_clause_value),
        )
        params = (entity_id, version)
    else:
        where_clause = sql.SQL(
            "WHERE {identifier_field} = %s ORDER BY {order_by_clause} DESC, id DESC"
            " LIMIT 1"
        ).format(
            identifier_field=sql.Identifier(table_details.identifier_field),
            order_by_clause=sql.Identifier(order_by_clause_value),
        )
        params = (entity_id,)

    query = (
        sql.SQL(
            """
        SELECT {fields}
        FROM {table}
        """
        ).format(
            fields=sql.SQL(",").join(map(sql.Identifier, columns)),
            table=sql.Identifier(table_details.table_name),
            identifier_field=sql.Identifier(table_details.identifier_field),
        )
        + where_clause
    )

    return query, params


def get_metadata_query(
    table_details: TableDetails, entity: OSOEntity
) -> QueryAndParameters:
    """
    Creates a query and parameters to find the latest metadata of the given entity in the table, returning the metadata columns if found.

    :param table_details: The information about the table to perform the update on.
    :param entity: The entity with an identifier to find the metadata for.
    :return: A tuple of the query and parameters, which psycopg will safely combine.
    """

    order_by_clause_value = get_order_by_clause_value_for_status_entity(table_details)

    query = sql.SQL(
        """
        SELECT {order_by_clause}, created_on, created_by, last_modified_on, last_modified_by
        FROM {table}
        WHERE {identifier_field} = %s
        ORDER BY {order_by_clause} DESC LIMIT 1
        """
    ).format(
        table=sql.Identifier(table_details.table_name),
        identifier_field=sql.Identifier(table_details.identifier_field),
        order_by_clause=sql.Identifier(order_by_clause_value),
    )

    params = (table_details.column_map[table_details.identifier_field](entity),)

    return query, params


def count_query(table_details: TableDetails) -> QueryAndParameters:
    query = sql.SQL("SELECT COUNT(*) FROM {table}").format(
        table=sql.Identifier(table_details.table_name)
    )
    return query, ()


def count_identifier_query(
    table_details: TableDetails, entity_id
) -> QueryAndParameters:
    query = sql.SQL(
        "SELECT COUNT(*) FROM {table} WHERE {identifier_field} = %s"
    ).format(
        table=sql.Identifier(table_details.table_name),
        identifier_field=sql.Identifier(table_details.identifier_field),
    )
    return query, (entity_id,)


def select_by_user_query(
    table_details: TableDetails, qry_params: UserQuery
) -> QueryAndParameters:
    columns = table_details.get_columns_with_metadata()

    match qry_params.match_type:
        case MatchType.EQUALS:
            where_clause = sql.SQL("WHERE created_by = %s")
            params = (qry_params.user,)
        case MatchType.STARTS_WITH:
            where_clause = sql.SQL("WHERE created_by LIKE %s")
            params = (f"{qry_params.user}%",)
        case MatchType.CONTAINS:
            where_clause = sql.SQL("WHERE created_by LIKE %s")
            params = (f"%{qry_params.user}%",)
        case _:
            raise QueryParameterError(
                message=(
                    f"Unsupported MatchType {qry_params.match_type.__class__.__name__}"
                )
            )

    query = (
        sql.SQL(
            """
        SELECT {fields}
        FROM {table}
        """
        ).format(
            fields=sql.SQL(",").join(map(sql.Identifier, columns)),
            table=sql.Identifier(table_details.table_name),
            identifier_field=sql.Identifier(table_details.identifier_field),
        )
        + where_clause
    )

    return query, params


def select_by_date_query(
    table_details: TableDetails, qry_params: DateQuery
) -> QueryAndParameters:
    columns = table_details.get_columns_with_metadata()
    if qry_params.start:
        if qry_params.end:
            where_clause = sql.SQL(
                """WHERE {date_field} >= %s AND {date_field} <= %s"""
            )
            params = (qry_params.start, qry_params.end)
        else:
            where_clause = sql.SQL(
                """
            WHERE {date_field} >= %s
            """
            )
            params = (qry_params.start,)
    else:
        where_clause = sql.SQL("""WHERE {date_field} <= %s""")
        params = (qry_params.end,)

    match qry_params.query_type:
        case DateQuery.QueryType.MODIFIED_BETWEEN:
            where_clause = where_clause.format(
                date_field=sql.Identifier("last_modified_on")
            )
        case DateQuery.QueryType.CREATED_BETWEEN:
            where_clause = where_clause.format(date_field=sql.Identifier("created_on"))
        case _:
            raise QueryParameterError(
                message=(
                    f"Unsupported query type {qry_params.query_type.__class__.__name__}"
                )
            )

    query = (
        sql.SQL(
            """
        SELECT {fields}
        FROM {table}
        """
        ).format(
            fields=sql.SQL(",").join(map(sql.Identifier, columns)),
            table=sql.Identifier(table_details.table_name),
        )
        + where_clause
    )

    return query, params


def select_status_by_id_query(
    table_details: TableDetails, qry_params: StatusQuery
) -> QueryAndParameters:
    columns = table_details.get_columns_with_metadata()

    order_by_clause_value = get_order_by_clause_value_for_status_entity(table_details)

    match qry_params.match_type:
        case MatchType.EQUALS:
            if qry_params.version:
                where_clause = sql.SQL(
                    "WHERE {identifier_field} LIKE %s and {order_by_clause} = %s"
                    " ORDER BY id"
                ).format(
                    identifier_field=sql.Identifier(table_details.identifier_field),
                    order_by_clause=sql.Identifier(order_by_clause_value),
                )
                params = (qry_params.entity_id, qry_params.version)
            else:
                where_clause = sql.SQL(
                    "WHERE {identifier_field} LIKE %s AND {order_by_clause} ="
                    " (SELECT {order_by_clause} FROM {table} WHERE"
                    " {identifier_field} LIKE %s  ORDER BY {order_by_clause} DESC"
                    " LIMIT 1 )  ORDER BY id"
                ).format(
                    identifier_field=sql.Identifier(table_details.identifier_field),
                    table=sql.Identifier(table_details.table_name),
                    order_by_clause=sql.Identifier(order_by_clause_value),
                )
                params = (qry_params.entity_id, qry_params.entity_id)
        case MatchType.STARTS_WITH:
            where_clause = sql.SQL("WHERE {identifier_field} LIKE %s").format(
                identifier_field=sql.Identifier(table_details.identifier_field)
            )
            params = (f"{qry_params.entity_id}%",)
        case MatchType.CONTAINS:
            where_clause = sql.SQL("WHERE {identifier_field} LIKE %s").format(
                identifier_field=sql.Identifier(table_details.identifier_field)
            )
            params = (f"%{qry_params.entity_id}%",)
        case _:
            raise QueryParameterError(
                message=(
                    f"Unsupported MatchType {qry_params.match_type.__class__.__name__}"
                )
            )

    query = (
        sql.SQL(
            """
        SELECT {fields}
        FROM {table}
        """
        ).format(
            fields=sql.SQL(",").join(map(sql.Identifier, columns)),
            table=sql.Identifier(table_details.table_name),
            identifier_field=sql.Identifier(table_details.identifier_field),
        )
        + where_clause
    )

    return query, params


def alias_identifier(ident, alias=None):
    """
    Method to create dynamic alias for secondary table columns
    :param indent: column name for alias
    :alias: alias name for provided column
    :return: sql Identifier for column along with alias
    """
    if isinstance(ident, str):
        ident = (ident,)
    if not alias:
        return sql.Identifier(*ident)
    else:
        return sql.Composed(
            [sql.Identifier(*ident), sql.SQL(" AS "), sql.Identifier(alias)]
        )


def select_latest_relationship_query(
    entity_id: str,
    parent_table_details: TableDetails,
    associated_table_details: TableDetails,
) -> QueryAndParameters:
    """
    Creates a query and parameters to find the latest version of the given entity in the table,
    returning the row if found. The query will join the secondary table with the primary table.
    Created alias for secondary table columns to avoid column name conflict.
    :param table_details: The information about the table to perform the update on.
    :param entity_id: The identifier of the entity to search for.
    :param parent_table_details: The primary entity table for join relationship.
    :param associated_table_details: secondary entity table which needs to be join.
    :return: A tuple of the query and parameters, which psycopg will safely combine.
    """

    columns = parent_table_details.get_columns_with_metadata()
    primary_table_columns = tuple(
        [parent_table_details.table_name + "." + column for column in list(columns)]
    )
    mapped_columns = associated_table_details.get_columns_with_metadata()
    mappend_alias_fields = [
        alias_identifier(
            [
                associated_table_details.table_name,
                col,
            ],
            alias=associated_table_details.table_name + "_" + col,
        )
        for col in list(mapped_columns)
    ]
    query_columns = [sql.Identifier(*f.split(".")) for f in primary_table_columns]
    query_columns.extend(mappend_alias_fields)

    query = sql.SQL(
        """
        SELECT {fields}
        FROM {table}
        RIGHT JOIN {joinee_table_name}
            ON {table}.{join_id} = {joinee_table_name}.{join_id}
        WHERE {identifier_field} = %s
        ORDER BY version DESC LIMIT 1
        """
    ).format(
        fields=sql.SQL(",").join(col for col in query_columns),
        table=sql.Identifier(parent_table_details.table_name),
        join_id=sql.Identifier(associated_table_details.identifier_field),
        joinee_table_name=sql.Identifier(associated_table_details.table_name),
        identifier_field=sql.Identifier(parent_table_details.identifier_field),
    )
    params = (entity_id,)
    return query, params


def result_to_metadata(query_result: Dict) -> Metadata:
    # Handling version key for status entity
    for key_version in list(query_result.keys()):
        if "version" in key_version:
            version_key = key_version

    return Metadata(
        version=query_result[version_key],
        created_on=query_result["created_on"],
        created_by=query_result["created_by"],
        last_modified_on=query_result["last_modified_on"],
        last_modified_by=query_result["last_modified_by"],
    )


def result_to_entity_relationship_metadata(query_result: Dict, table_name) -> Metadata:
    """
    Modified metadata using inner join query result metadata will be updated for secondary table.
    :param query_result: inner join query result
    :param table_name: secondary table name for inner join
    :return: updated metadata for secondary table
    """
    version = table_name + "_" + "version"
    created_on = table_name + "_" + "created_on"
    created_by = table_name + "_" + "created_by"
    last_modified_on = table_name + "_" + "last_modified_on"
    last_modified_by = table_name + "_" + "last_modified_by"
    return Metadata(
        version=query_result[version],
        created_on=query_result[created_on],
        created_by=query_result[created_by],
        last_modified_on=query_result[last_modified_on],
        last_modified_by=query_result[last_modified_by],
    )


def get_order_by_clause_value_for_status_entity(table_details: TableDetails) -> str:
    """This function returns the key from columns maps for status entity
    which will be used as order by clause and in select SQL queries.
    because status entities have reference of parent entity version as
    sbd_version, eb_version keys,
    In order to support versions in SQl query we generated order_by_clause
    using this method
    TODO: revisit this when version support is available for all the Entities
    :param table_details: TableDetails
    :return: str
    """

    column_map_keys = list(table_details.column_map.keys())

    if "status" in table_details.table_name:
        return next((key for key in column_map_keys if "version" in key), "version")
    else:
        return "version"
