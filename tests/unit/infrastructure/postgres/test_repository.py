"""
This module contains tests for the implementation of the Repository Bridge for Postgres.

The database connections and psycopg functionality is mocked out for the purpose of unit tests,
meaning these are quite lightweight. For more thorough tests of the functionality, see the integration tests.
"""

import json
from unittest import mock

import pytest
from ska_oso_pdm.entity_status_history import (
    OSOEBStatusHistory,
    ProjectStatusHistory,
    SBDStatusHistory,
    SBIStatusHistory,
)

from ska_db_oda.persistence.domain import get_identifier
from ska_db_oda.persistence.domain.errors import ODANotFound
from ska_db_oda.persistence.domain.query import StatusQuery, UserQuery
from ska_db_oda.persistence.infrastructure.postgres.mapping import (
    EntityRelationshipMapping,
    ExecutionBlockMapping,
    OSOExecutionBlockStatusHistoryMapping,
    ProjectMapping,
    ProjectStatusHistoryMapping,
    SBDefinitionMapping,
    SBDefinitionStatusHistoryMapping,
    SBInstanceMapping,
    SBInstanceStatusHistoryMapping,
)
from ska_db_oda.persistence.infrastructure.postgres.repository import PostgresBridge
from tests.conftest import TestDataFactory


@pytest.mark.parametrize(
    "mapping_cls,test_entity",
    [
        (SBDefinitionMapping, TestDataFactory.sbdefinition(sbd_id=None)),
        (SBInstanceMapping, TestDataFactory.sbinstance(sbi_id=None)),
        (ExecutionBlockMapping, TestDataFactory.executionblock(eb_id=None)),
        (ProjectMapping, TestDataFactory.project(prj_id=None)),
        # (ProposalMapping, TestDataFactory.proposal(prsl_id=None)), # TODO this test currently won't work as the Proposal can't be created without an ID
    ],
)
@mock.patch(
    "ska_db_oda.persistence.infrastructure.postgres.repository.PostgresBridge._execute_and_return_row"
)
@mock.patch("ska_db_oda.persistence.infrastructure.postgres.repository.insert_query")
@mock.patch(
    "ska_db_oda.persistence.infrastructure.postgres.repository.PostgresBridge._execute"
)
@mock.patch("ska_db_oda.persistence.domain.skuid.fetch_skuid")
def test_create(
    mock_skuid_fetch,
    mock_get_metadata_fn,
    mock_insert_query,
    mock_insert_fn,
    mapping_cls,
    test_entity,
):
    """
    When adding an entity, if the identifier is not present then it should be fetched from
    skuid before the relavent insert commands are called
    """
    test_entity_id = "test-id-123"
    mock_skuid_fetch.return_value = test_entity_id

    mock_get_metadata_fn.return_value = None
    mock_insert_query.return_value = ("qry", "param")
    mock_insert_fn.return_value = None

    repo_bridge = PostgresBridge(connection=mock.Mock, postgres_mapping=mapping_cls())

    repo_bridge.create(test_entity)

    mock_insert_fn.assert_called_once()
    mock_get_metadata_fn.assert_called_once()
    # the entity object should have been mutated to add an identifier
    assert test_entity_id == get_identifier(test_entity)


@pytest.mark.parametrize(
    "mapping_cls,test_entity",
    [
        (SBDefinitionMapping, TestDataFactory.sbdefinition()),
        (SBInstanceMapping, TestDataFactory.sbinstance()),
        (ExecutionBlockMapping, TestDataFactory.executionblock()),
        (ProjectMapping, TestDataFactory.project()),
    ],
)
@mock.patch(
    "ska_db_oda.persistence.infrastructure.postgres.repository.PostgresBridge._execute_and_return_row"
)
def test_read(mock_get_fn, mapping_cls, test_entity):
    """
    This test highlights that the current behaviour is to take all of the entity data from the jsonb rather than any
    database columns. They should always be the same, so it makes no difference, but the behaviour should be explicit and consistent.

    For example, with an SBDefinition, the sbd_id is extracted as a column for query performance
    and also stored within the jsonb. The get method will use this jsonb value.
    """

    mock_get_fn.return_value = {
        "info": test_entity,
        "version": 1,
        "created_on": test_entity.metadata.created_on,
        "created_by": test_entity.metadata.created_by,
        "last_modified_on": test_entity.metadata.last_modified_on,
        "last_modified_by": test_entity.metadata.last_modified_by,
    }

    repo_bridge = PostgresBridge(connection=mock.Mock, postgres_mapping=mapping_cls())

    result = repo_bridge.read(get_identifier(test_entity))

    assert result == test_entity

    mock_get_fn.assert_called_once()


@pytest.mark.parametrize(
    "mapping_cls,test_entity",
    [(EntityRelationshipMapping, TestDataFactory.ebassociatedwithsbi())],
)
@mock.patch(
    "ska_db_oda.persistence.infrastructure.postgres.repository.PostgresBridge._execute_and_return_row"
)
def test_read_with_inner_join(mock_get_fn, mapping_cls, test_entity):
    """
    This test highlights relationship between two tables to
    fetch data for associated_entity based on parent entity id
    """
    eb_entity, sbi_entity = test_entity
    mock_get_fn.return_value = {
        "info": json.loads(eb_entity.model_dump_json()),
        "version": 1,
        "created_on": eb_entity.metadata.created_on,
        "created_by": eb_entity.metadata.created_by,
        "last_modified_on": eb_entity.metadata.last_modified_on,
        "last_modified_by": eb_entity.metadata.last_modified_by,
        "tab_oda_sbi_info": json.loads(sbi_entity.model_dump_json()),
        "tab_oda_sbi_version": 1,
        "tab_oda_sbi_created_on": sbi_entity.metadata.created_on,
        "tab_oda_sbi_created_by": sbi_entity.metadata.created_by,
        "tab_oda_sbi_last_modified_on": sbi_entity.metadata.last_modified_on,
        "tab_oda_sbi_last_modified_by": sbi_entity.metadata.last_modified_by,
    }

    repo_bridge = PostgresBridge(connection=mock.Mock, postgres_mapping=mapping_cls())

    result = repo_bridge.read_relationship(get_identifier(eb_entity), "ebs", "sbis")

    assert result == sbi_entity

    mock_get_fn.assert_called_once()


@pytest.mark.parametrize(
    "mapping_cls,test_entity",
    [
        (SBDefinitionStatusHistoryMapping, TestDataFactory.sbd_status()),
        (SBInstanceStatusHistoryMapping, TestDataFactory.sbi_status()),
        (OSOExecutionBlockStatusHistoryMapping, TestDataFactory.eb_status()),
        (ProjectStatusHistoryMapping, TestDataFactory.prj_status()),
    ],
)
@mock.patch(
    "ska_db_oda.persistence.infrastructure.postgres.repository.PostgresBridge._execute_and_return_row"
)
def test_read_status_history(mock_get_fn, mapping_cls, test_entity):
    """
    This test highlights that the current behaviour is to take all of the entity data from the jsonb rather than any
    database columns. They should always be the same, so it makes no difference, but the behaviour should be explicit and consistent.

    For example, with an SBDefinition, the sbd_id is extracted as a column for query performance
    and also stored within the jsonb. The get method will use this jsonb value.
    """

    if isinstance(test_entity, OSOEBStatusHistory):
        mock_get_fn.return_value = {
            "eb_ref": test_entity.eb_ref,
            "eb_version": test_entity.eb_version,
            "current_status": test_entity.current_status,
            "previous_status": test_entity.previous_status,
            "created_on": test_entity.metadata.created_on,
            "created_by": test_entity.metadata.created_by,
            "last_modified_on": test_entity.metadata.last_modified_on,
            "last_modified_by": test_entity.metadata.last_modified_by,
        }

    elif isinstance(test_entity, SBDStatusHistory):
        mock_get_fn.return_value = {
            "sbd_ref": test_entity.sbd_ref,
            "sbd_version": test_entity.sbd_version,
            "current_status": test_entity.current_status,
            "previous_status": test_entity.previous_status,
            "created_on": test_entity.metadata.created_on,
            "created_by": test_entity.metadata.created_by,
            "last_modified_on": test_entity.metadata.last_modified_on,
            "last_modified_by": test_entity.metadata.last_modified_by,
        }

    elif isinstance(test_entity, SBIStatusHistory):
        mock_get_fn.return_value = {
            "sbi_ref": test_entity.sbi_ref,
            "sbi_version": test_entity.sbi_version,
            "current_status": test_entity.current_status,
            "previous_status": test_entity.previous_status,
            "created_on": test_entity.metadata.created_on,
            "created_by": test_entity.metadata.created_by,
            "last_modified_on": test_entity.metadata.last_modified_on,
            "last_modified_by": test_entity.metadata.last_modified_by,
        }

    elif isinstance(test_entity, ProjectStatusHistory):
        mock_get_fn.return_value = {
            "prj_ref": test_entity.prj_ref,
            "prj_version": test_entity.prj_version,
            "current_status": test_entity.current_status,
            "previous_status": test_entity.previous_status,
            "created_on": test_entity.metadata.created_on,
            "created_by": test_entity.metadata.created_by,
            "last_modified_on": test_entity.metadata.last_modified_on,
            "last_modified_by": test_entity.metadata.last_modified_by,
        }

    repo_bridge = PostgresBridge(connection=mock.Mock, postgres_mapping=mapping_cls())

    result = repo_bridge.read(get_identifier(test_entity))

    assert result == test_entity

    mock_get_fn.assert_called_once()


@pytest.mark.parametrize(
    "mapping_cls,test_entity",
    [
        (SBDefinitionMapping, TestDataFactory.sbdefinition()),
        (SBInstanceMapping, TestDataFactory.sbinstance()),
        (ExecutionBlockMapping, TestDataFactory.executionblock()),
        (ProjectMapping, TestDataFactory.project()),
    ],
)
@mock.patch(
    "ska_db_oda.persistence.infrastructure.postgres.repository.PostgresBridge._execute_and_return_rows"
)
def test_query_user(mock_get_fn, mapping_cls, test_entity):
    """
    This test is to get the Entity ID by querying the database on user field
    For example, with an SBDefinition, the sbd_id is returned when queried by user from the JSON
    """
    mock_get_fn.return_value = [{"info": test_entity}]

    repo_bridge = PostgresBridge(connection=mock.Mock, postgres_mapping=mapping_cls())

    result = repo_bridge.query(UserQuery(user="Test"))

    assert result[0] == test_entity

    mock_get_fn.assert_called_once()


@pytest.mark.parametrize(
    "mapping_cls,test_entity",
    [
        (SBDefinitionMapping, TestDataFactory.sbdefinition()),
        (SBInstanceMapping, TestDataFactory.sbinstance()),
        (ExecutionBlockMapping, TestDataFactory.executionblock()),
        (ProjectMapping, TestDataFactory.project()),
        (SBDefinitionStatusHistoryMapping, TestDataFactory.sbd_status()),
        (SBInstanceStatusHistoryMapping, TestDataFactory.sbi_status()),
        (OSOExecutionBlockStatusHistoryMapping, TestDataFactory.eb_status()),
        (ProjectStatusHistoryMapping, TestDataFactory.prj_status()),
    ],
)
@mock.patch(
    "ska_db_oda.persistence.infrastructure.postgres.repository.PostgresBridge._execute_and_return_rows"
)
def test_query_date(mock_get_fn, mapping_cls, test_entity):
    """
    This test is to get the Entity ID by querying the database on date field
    For example, with an SBDefinition, the sbd_id is returned when queried by date from the JSON
    """
    mock_get_fn.return_value = [{"info": test_entity}]

    repo_bridge = PostgresBridge(connection=mock.Mock, postgres_mapping=mapping_cls())

    result = repo_bridge.query(UserQuery(user="Test"))

    assert result[0] == test_entity

    mock_get_fn.assert_called_once()


@pytest.mark.parametrize(
    "mapping_cls,test_entity",
    [
        (SBDefinitionMapping, TestDataFactory.sbdefinition()),
        (SBInstanceMapping, TestDataFactory.sbinstance()),
        (ExecutionBlockMapping, TestDataFactory.executionblock()),
        (ProjectMapping, TestDataFactory.project()),
        (SBDefinitionStatusHistoryMapping, TestDataFactory.sbd_status()),
        (SBInstanceStatusHistoryMapping, TestDataFactory.sbi_status()),
        (OSOExecutionBlockStatusHistoryMapping, TestDataFactory.eb_status()),
        (ProjectStatusHistoryMapping, TestDataFactory.prj_status()),
    ],
)
@mock.patch(
    "ska_db_oda.persistence.infrastructure.postgres.repository.PostgresBridge._execute_and_return_row"
)
def test_get_not_found_returns_key_error(mock_get_fn, mapping_cls, test_entity):
    mock_get_fn.return_value = None

    repo_bridge = PostgresBridge(connection=mock.Mock, postgres_mapping=mapping_cls())

    with pytest.raises(ODANotFound):
        repo_bridge.read(get_identifier(test_entity))

    mock_get_fn.assert_called_once()


@pytest.mark.parametrize(
    "mapping_cls,test_entity",
    [
        (SBInstanceMapping, TestDataFactory.sbinstance()),
        (ExecutionBlockMapping, TestDataFactory.executionblock()),
    ],
)
@mock.patch(
    "ska_db_oda.persistence.infrastructure.postgres.repository.PostgresBridge._set_new_metadata"
)
@mock.patch("ska_db_oda.persistence.infrastructure.postgres.repository.update_query")
@mock.patch(
    "ska_db_oda.persistence.infrastructure.postgres.repository.PostgresBridge._execute_and_return_row"
)
def test_update_overwrites_first_version(
    mock_insert_fn, mock_update_query, mock_set_metadata, mapping_cls, test_entity
):
    """
    Test that updating an entity will overwrite the existing value, and not create a second version
    """
    mock_set_metadata.return_value = test_entity
    mock_update_query.return_value = ("qry", "param")
    mock_insert_fn.return_value = (1,)

    repo_bridge = PostgresBridge(connection=mock.Mock, postgres_mapping=mapping_cls())

    repo_bridge.update(test_entity)

    mock_set_metadata.assert_called_once()
    mock_update_query.assert_called_once()
    mock_insert_fn.assert_called_once()


@pytest.mark.parametrize(
    "mapping_cls,test_entity",
    [
        (SBInstanceMapping, TestDataFactory.sbinstance()),
        (ExecutionBlockMapping, TestDataFactory.executionblock()),
    ],
)
@mock.patch(
    "ska_db_oda.persistence.infrastructure.postgres.repository.PostgresBridge._set_new_metadata"
)
@mock.patch("ska_db_oda.persistence.infrastructure.postgres.repository.update_query")
@mock.patch(
    "ska_db_oda.persistence.infrastructure.postgres.repository.PostgresBridge._execute_and_return_row"
)
@mock.patch(
    "ska_db_oda.persistence.infrastructure.postgres.repository.PostgresBridge.create"
)
def test_update_creates_first_version(
    mock_create_fn,
    mock_insert_fn,
    mock_update_query,
    mock_set_metadata,
    mapping_cls,
    test_entity,
):
    """
    Test that updating an entity will create a first version if a row doesn't already exist
    """
    mock_set_metadata.return_value = test_entity
    mock_update_query.return_value = ("qry", "param")
    mock_insert_fn.return_value = None
    mock_create_fn.return_value = None

    repo_bridge = PostgresBridge(connection=mock.Mock, postgres_mapping=mapping_cls())

    repo_bridge.update(test_entity)

    mock_set_metadata.assert_called_once()
    mock_update_query.assert_called_once()
    mock_insert_fn.assert_called_once()
    mock_create_fn.assert_called_once()


@pytest.mark.parametrize(
    "mapping_cls,test_entity,pattern",
    [
        (SBDefinitionMapping, TestDataFactory.sbdefinition(), "sbd"),
        (SBInstanceMapping, TestDataFactory.sbinstance(), "sbi"),
        (ExecutionBlockMapping, TestDataFactory.executionblock(), "eb"),
        (ExecutionBlockMapping, TestDataFactory.project(), "prj"),
        (SBDefinitionStatusHistoryMapping, TestDataFactory.sbd_status(), "sbd"),
        (SBInstanceStatusHistoryMapping, TestDataFactory.sbi_status(), "sbi"),
        (OSOExecutionBlockStatusHistoryMapping, TestDataFactory.eb_status(), "eb"),
        (ProjectStatusHistoryMapping, TestDataFactory.prj_status(), "prj"),
    ],
)
@mock.patch(
    "ska_db_oda.persistence.infrastructure.postgres.repository.PostgresBridge._execute_and_return_rows"
)
def test_query_by_pattern_match(mock_get_fn, mapping_cls, test_entity, pattern):
    """
    This test is to search entity data on provided pattern
    """
    mock_get_fn.return_value = [{"info": test_entity}]

    repo_bridge = PostgresBridge(connection=mock.Mock, postgres_mapping=mapping_cls())

    result = repo_bridge.query(StatusQuery(entity_id=pattern))

    assert result[0] == test_entity

    mock_get_fn.assert_called_once()
