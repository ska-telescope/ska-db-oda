# pylint: disable=protected-access
import functools
import json
import logging
import os.path
from datetime import datetime
from pathlib import Path
from unittest import mock

import pytest
from dateutil.tz import tzlocal
from ska_oso_pdm import OSOExecutionBlock, Project, SBDefinition, SBInstance
from ska_oso_pdm._shared import Metadata
from ska_oso_pdm.entity_status_history import (
    OSOEBStatusHistory,
    SBDStatusHistory,
    SBIStatusHistory,
)

from ska_db_oda.persistence.domain import get_identifier
from ska_db_oda.persistence.domain.errors import (
    ODAError,
    ODANotFound,
    QueryParameterError,
)
from ska_db_oda.persistence.domain.query import (
    DateQuery,
    MatchType,
    StatusQuery,
    UserQuery,
)
from ska_db_oda.persistence.infrastructure.filesystem.mapping import (
    ExecutionBlockMapping,
    OSOExecutionBlockStatusHistoryMapping,
    ProjectMapping,
    SBDefinitionMapping,
    SBDefinitionStatusHistoryMapping,
    SBInstanceMapping,
    SBInstanceStatusHistoryMapping,
)
from ska_db_oda.persistence.infrastructure.filesystem.repository import (
    FilesystemBridge,
    QueryFilterFactory,
)
from tests.conftest import TestDataFactory

join_query_data = ("sbi", "sbd")

LOGGER = logging.getLogger(__name__)


@pytest.mark.parametrize(
    "mapping_cls",
    [
        ExecutionBlockMapping,
        ProjectMapping,
        SBDefinitionMapping,
        SBInstanceMapping,
        SBDefinitionStatusHistoryMapping,
        SBInstanceStatusHistoryMapping,
        OSOExecutionBlockStatusHistoryMapping,
    ],
)
def test_error_when_working_dir_is_missing(mapping_cls):
    """
    Verify that the FilesystemBridge complains if constructed with a
    missing working directory
    """
    missing_dir = Path("/foo")
    assert not missing_dir.exists()
    with pytest.raises(ODAError):
        FilesystemBridge(mapping_cls(), base_working_dir=missing_dir)


@pytest.mark.parametrize(
    "mapping_cls",
    [
        ExecutionBlockMapping,
        ProjectMapping,
        SBDefinitionMapping,
        SBInstanceMapping,
        SBDefinitionStatusHistoryMapping,
        SBInstanceStatusHistoryMapping,
        OSOExecutionBlockStatusHistoryMapping,
    ],
)
@mock.patch("ska_db_oda.persistence.infrastructure.filesystem.repository.access")
@mock.patch("ska_db_oda.persistence.infrastructure.filesystem.repository.Path.is_dir")
def test_error_when_working_dir_is_not_writable(
    mock_dir_fn, mock_access_fn, mapping_cls, base_working_dir
):
    """
    Verify that the FilesystemBridge complains if constructed with a
    non-writable working directory

    This test uses mocks as it is ran on an image with root privileges in the pipeline
    """
    ro_path = Path(base_working_dir) / "foo"
    mock_access_fn.side_effect = IOError("Directory not writable")
    mock_dir_fn.return_value = True
    with pytest.raises(IOError):
        _ = FilesystemBridge(mapping_cls(), base_working_dir=ro_path)


@pytest.mark.parametrize(
    "mapping_cls,entity_cls",
    [
        (SBDefinitionMapping, SBDefinition),
        (SBInstanceMapping, SBInstance),
        (ExecutionBlockMapping, OSOExecutionBlock),
        (ProjectMapping, Project),
        (SBDefinitionStatusHistoryMapping, SBDStatusHistory),
        (SBInstanceStatusHistoryMapping, SBIStatusHistory),
        (OSOExecutionBlockStatusHistoryMapping, OSOEBStatusHistory),
    ],
)
def test_get_entity_for_file_that_exists_retrieves_correct_entity(
    mapping_cls, entity_cls, base_working_dir
):
    """
    Confirm that getting an entity that exists in working directory is retrieved
    and returned correctly
    """
    repo_bridge = FilesystemBridge(mapping_cls(), base_working_dir)
    for f in repo_bridge.working_dir.glob("*.json"):
        expected = entity_cls.model_validate_json(f.read_text())
        retrieved = repo_bridge.read(get_identifier(expected))
        assert expected == retrieved


@pytest.mark.parametrize(
    "mapping_cls",
    [
        ExecutionBlockMapping,
        ProjectMapping,
        SBDefinitionMapping,
        SBInstanceMapping,
        SBDefinitionStatusHistoryMapping,
        SBInstanceStatusHistoryMapping,
        OSOExecutionBlockStatusHistoryMapping,
    ],
)
def test_get_nonexisting_entity_fails(mapping_cls, base_working_dir):
    """
    Confirm that retrieving an entity that doesn't exist raises an exception.
    """
    repo_bridge = FilesystemBridge(mapping_cls(), base_working_dir)
    entity_id = "entity-mvp01-20200325-20231"
    assert not Path(repo_bridge.working_dir, f"{entity_id}/1.json").exists()
    with pytest.raises(ODANotFound):
        _ = repo_bridge.read(entity_id)


@pytest.mark.parametrize(
    "mapping_cls,entity_dir,test_entity",
    [
        (SBDefinitionMapping, "sbd", TestDataFactory.sbdefinition()),
        (SBInstanceMapping, "sbi", TestDataFactory.sbinstance()),
        (ExecutionBlockMapping, "eb", TestDataFactory.executionblock()),
        (ProjectMapping, "prj", TestDataFactory.project()),
        (
            SBDefinitionStatusHistoryMapping,
            "sbd",
            TestDataFactory.sbd_status(),
        ),
        (
            SBInstanceStatusHistoryMapping,
            "sbi",
            TestDataFactory.sbi_status(),
        ),
        (
            OSOExecutionBlockStatusHistoryMapping,
            "eb",
            TestDataFactory.eb_status(),
        ),
    ],
)
def test_path_for(mapping_cls, entity_dir, test_entity, base_working_dir):
    """
    Validate path_for returns the expected path for the given entity.
    """
    if isinstance(
        test_entity, (SBIStatusHistory, SBDStatusHistory, OSOEBStatusHistory)
    ):
        expected = Path(
            base_working_dir,
            f"{entity_dir}/{get_identifier(test_entity)}/status_history_{test_entity.metadata.version}.json",
        )
        repo_bridge = FilesystemBridge(mapping_cls(), base_working_dir)
    else:
        expected = Path(
            base_working_dir,
            f"{entity_dir}/{get_identifier(test_entity)}/{test_entity.metadata.version}.json",
        )
        repo_bridge = FilesystemBridge(mapping_cls(), base_working_dir)
    assert repo_bridge._path_for_entity(test_entity) == expected


@pytest.mark.parametrize(
    "mapping_cls",
    [
        ExecutionBlockMapping,
        ProjectMapping,
        SBDefinitionMapping,
        SBInstanceMapping,
        SBDefinitionStatusHistoryMapping,
        SBInstanceStatusHistoryMapping,
        OSOExecutionBlockStatusHistoryMapping,
    ],
)
def test_len(mapping_cls, base_working_dir):
    """
    Verify that the length of the repo equals the JSON files in the working
    directory.
    """
    repo_bridge = FilesystemBridge(mapping_cls(), base_working_dir)

    def num_json_files():
        return sum(1 for _ in repo_bridge.working_dir.glob("*.json"))

    # assert length is as expected as we delete the JSON files one by one
    for f in repo_bridge.working_dir.glob("*.json"):
        assert len(repo_bridge) == num_json_files()
        f.unlink()


@pytest.mark.parametrize(
    "mapping_cls",
    [
        ExecutionBlockMapping,
        ProjectMapping,
        SBDefinitionMapping,
        SBInstanceMapping,
        SBDefinitionStatusHistoryMapping,
        SBInstanceStatusHistoryMapping,
        OSOExecutionBlockStatusHistoryMapping,
    ],
)
def test_contains(mapping_cls, base_working_dir):
    """
    Verify that an entity is considered included in a FilesystemBridge if a
    file with the expected filename is present in the working directory.
    """
    repo_bridge = FilesystemBridge(mapping_cls(), base_working_dir)
    for entity_path in repo_bridge.working_dir.glob("*.json"):
        expected_entity_id = entity_path.name.removesuffix(".json")
        assert expected_entity_id in repo_bridge


@pytest.mark.parametrize(
    "mapping_cls,test_entity",
    [
        (SBDefinitionMapping, TestDataFactory.sbdefinition()),
        (SBInstanceMapping, TestDataFactory.sbinstance()),
        (ExecutionBlockMapping, TestDataFactory.executionblock()),
        (ProjectMapping, TestDataFactory.project()),
    ],
)
@mock.patch("ska_db_oda.persistence.domain.metadatamixin.datetime")
def test_create_entity_file_system_repo(
    mock_datetime, mapping_cls, test_entity, base_working_dir
):
    """
    Verify that an entity created with the FilesystemBridge is serialised and stored in a
    file with the expected filename. Additionally, confirm that attempting to
    overwrite that entity results in an exception.
    """
    now = datetime.now(tz=tzlocal())
    mock_datetime.now.return_value = now

    repo_bridge = FilesystemBridge(mapping_cls(), base_working_dir)

    # new entity should not exist on filesystem
    new_entity_path = repo_bridge._path_for_entity(test_entity)
    assert not new_entity_path.exists()

    # first add should add serialised entity to pending transactions
    repo_bridge.create(test_entity)
    assert new_entity_path in repo_bridge._transactions

    test_entity.metadata = Metadata(
        version=1,
        created_on=now,
        created_by="DefaultUser",
        last_modified_on=now,
        last_modified_by="DefaultUser",
    )

    assert repo_bridge.read(get_identifier(test_entity)) == test_entity

    test_entity.metadata = Metadata(
        version=2,
        created_on=now,
        created_by="DefaultUser",
        last_modified_on=now,
        last_modified_by="DefaultUser",
    )

    # second add should create new version
    repo_bridge.create(test_entity)

    assert repo_bridge.read(get_identifier(test_entity)) == test_entity


@pytest.mark.parametrize(
    "mapping_cls,entity_dir",
    [
        (SBDefinitionMapping, "sbd"),
        (SBInstanceMapping, "sbi"),
        (ExecutionBlockMapping, "eb"),
        (ProjectMapping, "prj"),
        (SBDefinitionStatusHistoryMapping, "sbd"),
        (SBInstanceStatusHistoryMapping, "sbi"),
        (OSOExecutionBlockStatusHistoryMapping, "eb"),
    ],
)
def test_working_dir_priority(mapping_cls, entity_dir, base_working_dir):
    """
    Verify that the FilesystemBridge gets its working directory
    from, in order:

        1. ODA_DATA_DIR variable
        2. constructor argument
    """
    # environment variable has highest priority...
    with mock.patch.dict(os.environ, {"ODA_DATA_DIR": str(base_working_dir)}):
        repo_bridge = FilesystemBridge(mapping_cls(), "/bar")
        assert repo_bridge.working_dir == Path(base_working_dir) / entity_dir

    # ... followed by constructor argument
    repo_bridge = FilesystemBridge(mapping_cls(), base_working_dir)
    assert repo_bridge.working_dir == Path(base_working_dir) / entity_dir


@pytest.mark.parametrize(
    "mapping_cls,entity_dir",
    [
        (SBDefinitionMapping, "sbd"),
        (SBInstanceMapping, "sbi"),
        (ExecutionBlockMapping, "eb"),
        (ProjectMapping, "prj"),
        (SBDefinitionStatusHistoryMapping, "sbd"),
        (SBInstanceStatusHistoryMapping, "sbi"),
        (OSOExecutionBlockStatusHistoryMapping, "eb"),
    ],
)
@mock.patch("ska_db_oda.persistence.infrastructure.filesystem.repository.access")
@mock.patch("ska_db_oda.persistence.infrastructure.filesystem.repository.Path.is_dir")
@mock.patch("ska_db_oda.persistence.infrastructure.filesystem.repository.Path.mkdir")
def test_default_working_dir_argument(
    mock_mkdir_fn, mock_dir_fn, mock_access_fn, mapping_cls, entity_dir
):
    """
    Verify that the FilesystemRepository gets its working directory
    the default constructor argument if ODA_DATA_DIR is not set and an argument not passed

    This test uses mocks as it can't be guaranteed the default path will exist in the container that runs the test
    """
    mock_mkdir_fn.side_effect = None
    mock_access_fn.return_value = True
    mock_dir_fn.return_value = True

    repo_bridge = FilesystemBridge(mapping_cls())
    assert repo_bridge.working_dir == Path("/var/lib/oda") / entity_dir


@pytest.mark.parametrize(
    "mapping_cls,test_entity",
    [
        (SBDefinitionMapping, TestDataFactory.sbdefinition()),
        (SBInstanceMapping, TestDataFactory.sbinstance()),
        (ExecutionBlockMapping, TestDataFactory.executionblock()),
        (ProjectMapping, TestDataFactory.project()),
    ],
)
@mock.patch("ska_db_oda.persistence.domain.metadatamixin.datetime")
def test_when_entity_directory_is_empty(
    mock_datetime, base_working_dir, mapping_cls, test_entity
):
    """
    Test the case where a directory has been created for the entity id but an error with
    deserialization meant no entity version was saved. In this case the `__contains__` method
    for the entity id should return false (as no json files exist) and writing a new version 1 should be possible
    """

    now = datetime.now(tz=tzlocal())
    mock_datetime.now.return_value = now

    repo_bridge = FilesystemBridge(mapping_cls(), base_working_dir)
    Path(repo_bridge._path_for_entity_id_dir(get_identifier(test_entity))).mkdir(
        parents=True, exist_ok=True
    )

    # __contains__ should not return true as no versions exist
    assert get_identifier(test_entity) not in repo_bridge

    # add should write version 1 to filesystem
    repo_bridge.create(test_entity)

    test_entity.metadata = Metadata(
        version=1,
        created_on=now,
        created_by="DefaultUser",
        last_modified_on=now,
        last_modified_by="DefaultUser",
    )
    result = repo_bridge.read(get_identifier(test_entity))
    assert result == test_entity


@pytest.mark.parametrize(
    "mapping_cls,entity_generator,expected_type",
    [
        (
            SBDefinitionMapping,
            TestDataFactory.sbdefinition,
            # We cannot perform isinstance checks on annotated types such as
            # SBDefinitionID, giving the exception below. There's also no easy
            # way to extract the underlying type from an annotated type without
            # importing another library, so instead we just extract the base
            # type (=str) and test for that instead.
            #
            # TypeError: Subscripted generics cannot be used with class and instance checks
            str,
        ),
        (SBInstanceMapping, TestDataFactory.sbinstance, str),
        (ExecutionBlockMapping, TestDataFactory.executionblock, str),
        (ProjectMapping, TestDataFactory.project, str),
    ],
)
def test_get_entity_ids_is_of_correct_type(
    base_working_dir, mapping_cls, entity_generator, expected_type
):
    """
    Verify that getting all IDs for an SBRepository returns the expected result.
    """
    repo_bridge = FilesystemBridge(mapping_cls(), base_working_dir)

    expected_entity_ids = [f"test_entity_{i}" for i in range(10)]
    test_entities = [entity_generator(uid) for uid in expected_entity_ids]
    for entity in test_entities:
        repo_bridge.create(entity)

    actual = repo_bridge._all_entity_ids()
    assert all(isinstance(o, expected_type) for o in actual)
    assert sorted(actual) == sorted(expected_entity_ids)


@pytest.mark.parametrize(
    "mapping_cls,test_entity",
    [
        (SBDefinitionMapping, TestDataFactory.sbdefinition()),
        (SBInstanceMapping, TestDataFactory.sbinstance()),
        (ExecutionBlockMapping, TestDataFactory.executionblock()),
        (ProjectMapping, TestDataFactory.project()),
    ],
)
@mock.patch("ska_db_oda.persistence.domain.metadatamixin.datetime")
def test_update_overwrites_first_version(
    mock_datetime, base_working_dir, mapping_cls, test_entity
):
    """
    Test that updating an entity will overwrite the saved value, and not create a second version
    """
    now = datetime.now(tz=tzlocal())
    mock_datetime.now.return_value = now

    repo_bridge = FilesystemBridge(mapping_cls(), base_working_dir)
    repo_bridge.update(test_entity)

    test_entity.interface = "updated interface"
    repo_bridge.update(test_entity)

    expected_metadata = Metadata(
        version=1,
        created_by="DefaultUser",
        created_on=now,
        last_modified_on=now,
        last_modified_by="DefaultUser",
    )

    result = repo_bridge.read(get_identifier(test_entity))

    assert result.interface == "updated interface"
    assert result.metadata == expected_metadata
    assert len(repo_bridge._transactions) == 1


@pytest.mark.parametrize(
    "mapping_cls,test_entity",
    [
        (SBDefinitionMapping, TestDataFactory.sbdefinition(sbd_id=None)),
        (SBInstanceMapping, TestDataFactory.sbinstance(sbi_id=None)),
        (ExecutionBlockMapping, TestDataFactory.executionblock(eb_id=None)),
        (ProjectMapping, TestDataFactory.project(prj_id=None)),
    ],
)
@mock.patch("ska_db_oda.persistence.domain.skuid.fetch_skuid")
def test_id_fetched_from_skuid_if_not_present(
    mock_skuid_fetch, base_working_dir, mapping_cls, test_entity
):
    """
    When adding an entity, if the identifier is not present then it should be fetched from skuid
    """
    # Arrange
    test_entity_id = "test-id-123"
    mock_skuid_fetch.return_value = test_entity_id

    repo_bridge = FilesystemBridge(mapping_cls(), base_working_dir)
    # Act
    repo_bridge.create(test_entity)

    # Assert - the entity object should have been mutated to add an identifier
    assert test_entity_id == get_identifier(test_entity)


class TestQueryFilterFactory:
    """
    Unit tests for the QueryFilterFactory.
    """

    @pytest.mark.parametrize(
        "query,expected",
        [
            (UserQuery(user="foo"), False),
            (UserQuery(user=TestDataFactory.sbdefinition().metadata.created_by), True),
            (
                UserQuery(
                    user=TestDataFactory.sbdefinition().metadata.created_by[0:4],
                    match_type=MatchType.STARTS_WITH,
                ),
                True,
            ),
            (UserQuery(user="foo", match_type=MatchType.STARTS_WITH), False),
        ],
    )
    def test_query_by_author(self, query, expected):
        sbd = TestDataFactory.sbdefinition()
        match_fn = QueryFilterFactory.match_editor(query)
        assert match_fn(sbd) == expected

    # an exact date specified in an entity metadata block used to test bounds
    CREATED_ON = TestDataFactory.sbdefinition().metadata.created_on
    # arbitrary date in the past
    THE_PAST = datetime.fromisoformat("1970-01-01T00:00:00.000000+00:00")
    # arbitrary date in the future
    THE_FUTURE = datetime.fromisoformat("2970-01-01T00:00:00.000000+00:00")

    @pytest.mark.parametrize(
        "query,expected",
        [
            (
                DateQuery(
                    query_type=DateQuery.QueryType.CREATED_BETWEEN, start=THE_PAST
                ),
                True,
            ),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.CREATED_BETWEEN, start=CREATED_ON
                ),
                True,
            ),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.CREATED_BETWEEN, start=THE_FUTURE
                ),
                False,
            ),
            (
                DateQuery(query_type=DateQuery.QueryType.CREATED_BETWEEN, end=THE_PAST),
                False,
            ),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.CREATED_BETWEEN, end=CREATED_ON
                ),
                False,
            ),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.CREATED_BETWEEN, end=THE_FUTURE
                ),
                True,
            ),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.CREATED_BETWEEN,
                    start=THE_PAST,
                    end=THE_FUTURE,
                ),
                True,
            ),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.CREATED_BETWEEN,
                    start=THE_PAST,
                    end=CREATED_ON,
                ),
                False,
            ),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.CREATED_BETWEEN,
                    start=CREATED_ON,
                    end=THE_FUTURE,
                ),
                True,
            ),
        ],
    )
    def test_query_by_creation_date(self, query, expected):
        sbd = TestDataFactory.sbdefinition()
        match_fn = QueryFilterFactory.filter_between_dates(query)
        assert match_fn(sbd) == expected

    @pytest.mark.parametrize(
        "query,expected",
        [
            (
                DateQuery(
                    query_type=DateQuery.QueryType.MODIFIED_BETWEEN, start=THE_PAST
                ),
                True,
            ),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.MODIFIED_BETWEEN, start=CREATED_ON
                ),
                True,
            ),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.MODIFIED_BETWEEN, start=THE_FUTURE
                ),
                False,
            ),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.MODIFIED_BETWEEN, end=THE_PAST
                ),
                False,
            ),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.MODIFIED_BETWEEN, end=CREATED_ON
                ),
                False,
            ),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.MODIFIED_BETWEEN, end=THE_FUTURE
                ),
                True,
            ),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.MODIFIED_BETWEEN,
                    start=THE_PAST,
                    end=THE_FUTURE,
                ),
                True,
            ),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.MODIFIED_BETWEEN,
                    start=THE_PAST,
                    end=CREATED_ON,
                ),
                False,
            ),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.MODIFIED_BETWEEN,
                    start=CREATED_ON,
                    end=THE_FUTURE,
                ),
                True,
            ),
        ],
    )
    def test_query_by_modification_date(self, query, expected):
        sbd = TestDataFactory.sbdefinition()
        # set modification date different from creation date. This should be
        # enough to test that we are comparing different attributes
        sbd.metadata.created_on = TestQueryFilterFactory.THE_FUTURE
        match_fn = QueryFilterFactory.filter_between_dates(query)
        assert match_fn(sbd) == expected

    @pytest.mark.parametrize(
        "query",
        [
            (DateQuery(query_type=DateQuery.QueryType.CREATED_BETWEEN)),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.CREATED_BETWEEN,
                    start=THE_FUTURE,
                    end=THE_PAST,
                )
            ),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.CREATED_BETWEEN,
                    start=THE_FUTURE,
                    end=THE_FUTURE,
                )
            ),
            (DateQuery(query_type=DateQuery.QueryType.MODIFIED_BETWEEN)),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.MODIFIED_BETWEEN,
                    start=THE_FUTURE,
                    end=THE_PAST,
                )
            ),
            (
                DateQuery(
                    query_type=DateQuery.QueryType.MODIFIED_BETWEEN,
                    start=THE_FUTURE,
                    end=THE_FUTURE,
                )
            ),
        ],
    )
    def test_match_dates_raises_exception_for_invalid_query(self, query):
        with pytest.raises(QueryParameterError):
            _ = QueryFilterFactory.filter_between_dates(query)


class TestQueryRepository:
    """
    Test that repository query works as intended.
    """

    @pytest.mark.parametrize(
        "mapping_cls,test_entity_generator,query, entity_id",
        [
            (
                SBDefinitionMapping,
                TestDataFactory.sbdefinition,
                UserQuery(user="foo"),
                "sbd_id",
            ),
            (
                SBDefinitionMapping,
                TestDataFactory.sbdefinition,
                UserQuery(user="DefaultUser"),
                "sbd_id",
            ),
            (
                SBDefinitionMapping,
                TestDataFactory.sbdefinition,
                UserQuery(user="Def", match_type=MatchType.STARTS_WITH),
                "sbd_id",
            ),
            (
                SBDefinitionMapping,
                TestDataFactory.sbdefinition,
                UserQuery(user="foo", match_type=MatchType.STARTS_WITH),
                "sbd_id",
            ),
            (
                SBInstanceMapping,
                TestDataFactory.sbinstance,
                UserQuery(user="foo"),
                "sbi_id",
            ),
            (
                SBInstanceMapping,
                TestDataFactory.sbinstance,
                UserQuery(user="DefaultUser"),
                "sbi_id",
            ),
            (
                SBInstanceMapping,
                TestDataFactory.sbinstance,
                UserQuery(user="Def", match_type=MatchType.STARTS_WITH),
                "sbi_id",
            ),
            (
                SBInstanceMapping,
                TestDataFactory.sbinstance,
                UserQuery(user="foo", match_type=MatchType.STARTS_WITH),
                "sbi_id",
            ),
            (
                ExecutionBlockMapping,
                TestDataFactory.executionblock,
                UserQuery(user="foo"),
                "eb_id",
            ),
            (
                ExecutionBlockMapping,
                TestDataFactory.executionblock,
                UserQuery(user="DefaultUser"),
                "eb_id",
            ),
            (
                ExecutionBlockMapping,
                TestDataFactory.executionblock,
                UserQuery(user="Def", match_type=MatchType.STARTS_WITH),
                "eb_id",
            ),
            (
                ExecutionBlockMapping,
                TestDataFactory.executionblock,
                UserQuery(user="foo", match_type=MatchType.STARTS_WITH),
                "eb_id",
            ),
            (ProjectMapping, TestDataFactory.project, UserQuery(user="foo"), "prj_id"),
            (
                ProjectMapping,
                TestDataFactory.project,
                UserQuery(user="DefaultUser"),
                "prj_id",
            ),
            (
                ProjectMapping,
                TestDataFactory.project,
                UserQuery(user="Def", match_type=MatchType.STARTS_WITH),
                "prj_id",
            ),
            (
                ProjectMapping,
                TestDataFactory.project,
                UserQuery(user="foo", match_type=MatchType.STARTS_WITH),
                "prj_id",
            ),
            (
                SBDefinitionMapping,
                TestDataFactory.sbdefinition,
                StatusQuery(entity_id="ty-0"),
                "sbd_id",
            ),
            (
                SBInstanceMapping,
                TestDataFactory.sbinstance,
                StatusQuery(entity_id="ty-0"),
                "sbi_id",
            ),
            (
                ExecutionBlockMapping,
                TestDataFactory.executionblock,
                StatusQuery(entity_id="ty-0"),
                "eb_id",
            ),
            (
                ProjectMapping,
                TestDataFactory.project,
                StatusQuery(entity_id="ty-0"),
                "prj_id",
            ),
            (
                SBDefinitionMapping,
                TestDataFactory.sbdefinition,
                StatusQuery(entity_id="ent"),
                "sbd_id",
            ),
            (
                SBInstanceMapping,
                TestDataFactory.sbinstance,
                StatusQuery(entity_id="ent"),
                "sbi_id",
            ),
            (
                ExecutionBlockMapping,
                TestDataFactory.executionblock,
                StatusQuery(entity_id="ent"),
                "eb_id",
            ),
            (
                ProjectMapping,
                TestDataFactory.project,
                StatusQuery(entity_id="ent"),
                "prj_id",
            ),
        ],
    )
    @mock.patch("ska_db_oda.persistence.domain.metadatamixin.datetime")
    def test_query(
        self,
        mock_datetime,
        mapping_cls,
        test_entity_generator,
        query,
        entity_id,
        base_working_dir,
    ):
        """
        Verify that an entity added to the FilesystemBridge is serialized and stored in a
        file with the expected filename. Additionally, confirm that attempting to
        overwrite that entity results in an exception.
        """
        dt = datetime.fromisoformat("2000-11-22T11:22:33.444444+00:00")
        mock_datetime.now.return_value = dt

        repo_bridge = FilesystemBridge(mapping_cls(), base_working_dir)

        for i in range(3):
            entity = test_entity_generator(f"entity-{i}")
            repo_bridge.create(entity)
        results = repo_bridge.query(query)
        for res in results:
            entity_json_obj = json.loads(res.model_dump_json())
            entity = test_entity_generator(entity_json_obj[entity_id])
            entity.metadata = Metadata(
                version=1,
                created_on=dt,
                created_by="DefaultUser",
                last_modified_on=dt,
                last_modified_by="DefaultUser",
            )

            # Equivalent timezones are not considered equal
            # https://github.com/pydantic/pydantic/issues/8683
            #
            # Equivalent timezones should have same ISO representation, so
            # assert that instead
            for dt_attr in ["metadata.created_on", "metadata.last_modified_on"]:
                entity_dt = rgetattr(entity, dt_attr)
                res_dt = rgetattr(res, dt_attr)
                assert entity_dt.isoformat() == res_dt.isoformat()
                rsetattr(entity, dt_attr, entity_dt.replace(tzinfo=res_dt.tzinfo))

            # comparing entire inserted object
            assert res == entity


# Utility functions to assist with Pydantic tzinfo inequality workaround
def rgetattr(obj, attr, *args):
    def _getattr(obj, attr):
        return getattr(obj, attr, *args)

    return functools.reduce(_getattr, [obj] + attr.split("."))


def rsetattr(obj, attr, val):
    pre, _, post = attr.rpartition(".")
    return setattr(rgetattr(obj, pre) if pre else obj, post, val)
