from unittest import mock

import pytest
from ska_oso_pdm._shared import TelescopeType
from ska_oso_pdm.execution_block import OSOExecutionBlock as ExecutionBlock
from ska_oso_pdm.project import Project
from ska_oso_pdm.proposal import Proposal
from ska_oso_pdm.sb_definition import SBDefinition
from ska_oso_pdm.sb_instance import SBInstance

from ska_db_oda.client.odacli import (
    EBClient,
    OSOEBStatusHistoryClient,
    ProjClient,
    ProjectStatusHistoryClient,
    ProposalClient,
    SBDClient,
    SBDStatusHistoryClient,
    SBIClient,
    SBIStatusHistoryClient,
)

MOCK_ODA_URL = "http://foo/oda"


@pytest.mark.parametrize(
    "cli_cls,id_field,expected_entity",
    [
        (
            SBDClient,
            "sbd_id",
            SBDefinition(sbd_id="abd-123", interface="http://fake-interface"),
        ),
        (
            SBIClient,
            "sbi_id",
            SBInstance(sbi_id="sbi-123", telescope=TelescopeType.SKA_MID),
        ),
        (
            EBClient,
            "eb_id",
            ExecutionBlock(eb_id="eb-123", telescope=TelescopeType.SKA_MID),
        ),
        (
            ProjClient,
            "prj_id",
            Project(prj_id="prj-123", telescope=TelescopeType.SKA_MID),
        ),
        (
            ProposalClient,
            "prsl_id",
            Proposal(
                prsl_id="prsl-123",
                cycle="1234",
                info={
                    "proposal_type": {
                        "main_type": "standard_proposal",
                        "attributes": ["coordinated_proposal"],
                    },
                    "title": "dummy prsl title",
                },
            ),
        ),
    ],
)
def test_absrepoclient_get(cli_cls, id_field, expected_entity):
    client = cli_cls(MOCK_ODA_URL)
    expected_id = getattr(expected_entity, id_field)
    client._get = mock.Mock(  # pylint: disable=protected-access
        return_value=expected_entity
    )
    entity_output = client.get(expected_id)

    # CLI should format everything into a string to display to the user
    assert isinstance(entity_output, str)
    # Output string should mention the entity ID
    assert expected_id in entity_output


@pytest.mark.parametrize(
    "cli_cls,entity_ids",
    [
        (SBDClient, ["sbd-123", "sbd-234", "sbd-345"]),
        (SBIClient, ["sbi-123", "sbi-234", "sbi-345"]),
        (EBClient, ["eb-123", "eb-234", "eb-345"]),
        (ProjClient, ["prj-123", "prj-234", "prj-345"]),
        (SBDStatusHistoryClient, ["sbd-123", "sbd-234", "sbd-345"]),
        (SBIStatusHistoryClient, ["sbi-123", "sbi-234", "sbi-345"]),
        (OSOEBStatusHistoryClient, ["eb-123", "eb-234", "eb-345"]),
        (ProjectStatusHistoryClient, ["prj-123", "prj-234", "prj-345"]),
    ],
)
def test_absrepoclient_query(cli_cls, entity_ids):
    client = cli_cls(MOCK_ODA_URL)
    client._query = mock.Mock(  # pylint: disable=protected-access
        return_value=entity_ids
    )
    query_output = client.query(user="user1")

    # CLI should format everything into a string to display to the user
    assert isinstance(query_output, str)
    # Output string should mention all the returned IDs
    assert all([eid in query_output for eid in entity_ids])
