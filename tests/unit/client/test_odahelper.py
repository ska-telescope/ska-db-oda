import json
import os
from datetime import datetime, timedelta, timezone
from http import HTTPStatus
from unittest import TestCase, mock

import pytest
import requests_mock
from requests.exceptions import HTTPError
from ska_oso_pdm._shared import TelescopeType

from ska_db_oda.client import oda_helper
from ska_db_oda.constant import NOT_FOUND_MESSAGE
from ska_db_oda.persistence.domain import CODEC
from tests.conftest import DEFAULT_API_PATH, TestDataFactory

TEST_DATETIME = datetime.now(tz=timezone(offset=timedelta(hours=3)))

TEST_ODA_URL = f"http://mocked/{DEFAULT_API_PATH}"


@oda_helper.capture_request_response
def dummy_function_to_decorate(first_param: int, second_param: str):
    return f"the test function is called with {first_param} and {second_param}"


@oda_helper.capture_request_response
def dummy_error_function_to_decorate(first_param: int, second_param: str):
    raise RuntimeError(
        f"this is an error in the test function called with {first_param} and"
        f" {second_param}"
    )


class TestCaptureRequestResponse(TestCase):
    @mock.patch("ska_db_oda.client.oda_helper.datetime")
    @mock.patch.dict(os.environ, {"ODA_URL": TEST_ODA_URL, "EB_ID": "eb-123"})
    def test_eb_decorator_sends_request_response(self, mock_datetime):
        """
        Tests the happy path where the environment variables are set, the ODA is available and the decorated function
        returns a value.
        """

        mock_datetime.now.return_value = TEST_DATETIME

        request_response = {
            "request": "tests.unit.client.test_odahelper.dummy_function_to_decorate",
            "request_args": {"args": [1], "kwargs": {"second_param": "test"}},
            "status": "OK",
            "response": {"result": "'the test function is called with 1 and test'"},
            "request_sent_at": TEST_DATETIME.isoformat(),
            "response_received_at": TEST_DATETIME.isoformat(),
        }

        with requests_mock.Mocker() as mock_server:
            mock_server.patch(
                f"{TEST_ODA_URL}/ebs/eb-123/request_response",
                status_code=HTTPStatus.OK,
            )

            # Call the function which has been decorated
            result = dummy_function_to_decorate(1, second_param="test")

            history = mock_server.request_history

            assert len(history) == 1
            assert json.loads(history[0].text) == request_response
            assert result == "the test function is called with 1 and test"

    @mock.patch("ska_db_oda.client.oda_helper.datetime")
    @mock.patch.dict(os.environ, {"ODA_URL": TEST_ODA_URL, "EB_ID": "eb-123"})
    def test_when_function_raises_exception(self, mock_datetime):
        """
        Tests the path where the environment variables are set, the ODA is available but the function
        raises an error.
        The error should still be raised by the function with the decorator applied and should be recorded in the ODA.
        """
        mock_datetime.now.return_value = TEST_DATETIME

        request_response = {
            "request": (
                "tests.unit.client.test_odahelper.dummy_error_function_to_decorate"
            ),
            "request_args": {"args": [1, "input"]},
            "status": "ERROR",
            "error": {
                "detail": (
                    "RuntimeError('this is an error in the test function called with 1"
                    " and input')"
                )
            },
            "request_sent_at": TEST_DATETIME.isoformat(),
        }

        with requests_mock.Mocker() as mock_server:
            mock_server.patch(
                f"{TEST_ODA_URL}/ebs/eb-123/request_response",
                status_code=HTTPStatus.OK,
            )

            # Call the function which has been decorated
            with pytest.raises(RuntimeError) as err:
                dummy_error_function_to_decorate(1, "input")

            assert repr(err.value) == repr(
                RuntimeError(
                    "this is an error in the test function called with 1 and input"
                )
            )

            history = mock_server.request_history

            # removing key `response_received_at` from json_data
            updated_data = {
                key: value
                for key, value in json.loads(history[0].text).items()
                if key != "response_received_at"
            }

            assert len(history) == 1
            assert updated_data == request_response

    def test_error_log_when_oda_url_not_set(self):
        """
        Tests the path where the ODA_URL environment variable is not set.
        An error message should be logged but the decorated function should still return its usual value.
        """
        with self.assertLogs(level="ERROR") as logs:
            result = dummy_function_to_decorate(1, second_param="test")
            self.assertEqual(
                logs.output,
                [
                    "ERROR:ska_db_oda.client.oda_helper:The ODA_URL not set, meaning"
                    " Execution Block updates will not be sent to the ODA. Please set"
                    " this variable to the URL for an instance of the ODA, eg"
                    " ODA_URL=http://k8s.stfc.skao.int/staging-ska-db-oda/"
                ],
            )
            # The function should still execute as normal rather than failing, but the ODA request will not have been sent
            assert result == "the test function is called with 1 and test"

    @mock.patch("ska_db_oda.client.oda_helper.datetime")
    @mock.patch.dict(os.environ, {"ODA_URL": f"{TEST_ODA_URL}"})
    def test_error_log_when_eb_id_not_set(self, mock_datetime):
        """
        Tests the path where the EB_ID environment variable is not set.
        An error message should be logged but the decorated function should still return its usual value.
        """
        mock_datetime.now.return_value = TEST_DATETIME
        with self.assertLogs(level="ERROR") as logs:
            result = dummy_function_to_decorate(1, second_param="test")
            self.assertEqual(
                logs.output,
                [
                    "ERROR:ska_db_oda.client.oda_helper:The EB_ID not set, meaning"
                    " Execution Block updates will not be sent to the ODA. Please"
                    " ensure the create function from the EB ODA client has been"
                    " called."
                ],
            )
            # The function should still execute as normal rather than failing, but the ODA request will not have been sent
            assert result == "the test function is called with 1 and test"

    @mock.patch("ska_db_oda.client.oda_helper.datetime")
    @mock.patch.dict(os.environ, {"ODA_URL": TEST_ODA_URL, "EB_ID": "eb-123"})
    def test_error_log_when_oda_request_raises_error(self, mock_datetime):
        """
        Tests the path where the environment variables are set but the request to the ODA raises an error.
        An error message should be logged but the decorated function should still return its usual value.
        """
        mock_datetime.now.return_value = TEST_DATETIME
        with mock.patch("requests.patch") as mock_patch:
            mock_patch.side_effect = HTTPError("requests.patch raised error")
            with self.assertLogs(level="ERROR") as logs:
                result = dummy_function_to_decorate(1, second_param="test")
                self.assertEqual(
                    logs.output,
                    [
                        f"""ERROR:ska_db_oda.client.oda_helper:The request to {TEST_ODA_URL}/ebs/eb-123/request_response raised an error: HTTPError('requests.patch raised error')"""
                    ],
                )
                # The function should still execute as normal rather than failing
                assert result == "the test function is called with 1 and test"

    @mock.patch("ska_db_oda.client.oda_helper.datetime")
    @mock.patch.dict(os.environ, {"ODA_URL": TEST_ODA_URL, "EB_ID": "eb-123"})
    def test_error_log_when_oda_returns_error_response(self, mock_datetime):
        """
        Tests the path where the environment variables are set but the ODA returns an error.
        An error message should be logged but the decorated function should still return its usual value.
        """
        mock_datetime.now.return_value = TEST_DATETIME
        with requests_mock.Mocker() as mock_server:
            mock_server.patch(
                f"{TEST_ODA_URL}/ebs/eb-123/request_response",
                status_code=HTTPStatus.INTERNAL_SERVER_ERROR,
                content=bytes(json.dumps({"detail": "this is an ODA error"}), "utf-8"),
            )
            with self.assertLogs(level="ERROR") as logs:
                result = dummy_function_to_decorate(1, second_param="test")
                self.assertEqual(
                    logs.output,
                    [
                        "ERROR:ska_db_oda.client.oda_helper:The request to"
                        f" http://mocked/{DEFAULT_API_PATH}/ebs/eb-123/request_response "
                        + """returned an error code 500, with message: b\'{"detail": "this is an ODA error"}\'"""
                    ],
                )
                # The function should still execute as normal rather than failing
                assert result == "the test function is called with 1 and test"
                assert len(mock_server.request_history) == 1


class TestCreateEB:
    @mock.patch.dict(os.environ, {"ODA_URL": TEST_ODA_URL})
    def test_eb_is_created_and_id_variable_set(self):
        """
        Tests that the function makes a request to the correct API, sets the environment variable and returns the value
        """
        with requests_mock.Mocker() as mock_server:
            mock_server.post(
                f"{TEST_ODA_URL}/ebs",
                status_code=HTTPStatus.OK,
                content=bytes(
                    json.dumps({"eb_id": "created-eb-123", "telescope": "ska_low"}),
                    "utf-8",
                ),
            )
            eb_id = oda_helper.create_eb(telescope=TelescopeType.SKA_LOW)

            assert len(mock_server.request_history) == 1

        assert eb_id == "created-eb-123"
        assert os.getenv("EB_ID") == "created-eb-123"

    def test_key_error_raised_if_oda_url_not_set(self):
        with pytest.raises(KeyError) as err:
            oda_helper.create_eb(telescope=TelescopeType.SKA_LOW)

        assert (
            err.value.args[0]
            == "ODA_URL environment variable is not set. Please set to a running"
            " instance of the ODA, eg"
            " ODA_URL=https://k8s.stfc.skao.int/staging-ska-db-oda/oda/api/v1/"
        )

    @mock.patch.dict(os.environ, {"ODA_URL": f"{TEST_ODA_URL}"})
    def test_error_http_response_is_handled(self):
        """
        Tests that any error response from the API is raised as a ConnectionError
        """
        with requests_mock.Mocker() as mock_server:
            mock_server.post(
                f"{TEST_ODA_URL}/ebs",
                status_code=HTTPStatus.INTERNAL_SERVER_ERROR,
                content=bytes(
                    json.dumps({"detail": "error returned from ODA"}), "utf-8"
                ),
            )
            with pytest.raises(ConnectionError) as err:
                oda_helper.create_eb(telescope=TelescopeType.SKA_LOW)

            assert len(mock_server.request_history) == 1
            assert (
                err.value.args[0]
                == "Response status 500 with body 'error returned from ODA' while"
                f" sending post request to {TEST_ODA_URL}/ebs"
            )

    @mock.patch.dict(os.environ, {"ODA_URL": f"{TEST_ODA_URL}/"})
    def test_error_from_requests_is_handled(self):
        """
        Tests the path where the request.post call raises and error, which should be raised as a ConnectionError
        """
        with mock.patch("requests.post") as mock_post:
            mock_post.side_effect = HTTPError("requests.post raised error")
            with pytest.raises(ConnectionError) as err:
                oda_helper.create_eb(telescope=TelescopeType.SKA_LOW)

            assert (
                err.value.args[0]
                == "Error 'HTTPError('requests.post raised error')' while sending post"
                f" request to {TEST_ODA_URL}/ebs"
            )


class TestSaveSBD:
    def test_key_error_raised_if_oda_url_not_set(self):
        with pytest.raises(KeyError) as err:
            oda_helper.save(TestDataFactory.sbdefinition())

        assert (
            err.value.args[0]
            == "ODA_URL environment variable is not set. Please set to a running"
            " instance of the ODA, eg"
            " ODA_URL=https://k8s.stfc.skao.int/staging-ska-db-oda/oda/api/v1/"
        )

    @mock.patch.dict(os.environ, {"ODA_URL": f"{TEST_ODA_URL}"})
    def test_sbd_created_successfully(self):
        """
        Tests that the function makes a request to the correct API and returns the persisted SBD.

        In this case as the sbd_id is None, the save function will create a new SBDefinition via the POST endpoint.
        """
        # Arrange
        test_sbd = TestDataFactory.sbdefinition(sbd_id=None)
        with requests_mock.Mocker() as mock_server:
            mock_server.post(
                f"{TEST_ODA_URL}/sbds",
                status_code=HTTPStatus.OK,
                content=bytes(CODEC.dumps(test_sbd), "utf-8"),
            )

            # Act
            result = oda_helper.save(test_sbd)

            # Assert
            assert len(mock_server.request_history) == 1
            assert result == test_sbd

    @mock.patch.dict(os.environ, {"ODA_URL": f"{TEST_ODA_URL}"})
    def test_sbd_updated_successfully(self):
        """
        Tests that the function makes a request to the correct API and returns the persisted SBD.

        In this case as the sbd_id is given, the save function will update the  SBDefinition via the OUT endpoint.
        """
        # Arrange
        test_sbd = TestDataFactory.sbdefinition(sbd_id="sbd-test-1234")
        with requests_mock.Mocker() as mock_server:
            mock_server.put(
                f"{TEST_ODA_URL}/sbds/{test_sbd.sbd_id}",
                status_code=HTTPStatus.OK,
                content=bytes(CODEC.dumps(test_sbd), "utf-8"),
            )

            # Act
            result = oda_helper.save(test_sbd)

            # Assert
            assert len(mock_server.request_history) == 1
            assert result == test_sbd

    @mock.patch.dict(os.environ, {"ODA_URL": f"{TEST_ODA_URL}"})
    def test_bad_request_is_handled(self):
        """
        Tests that the error response from the API is raised as a ConnectionError in the function
        """
        with requests_mock.Mocker() as mock_server:
            mock_server.post(
                f"{TEST_ODA_URL}/sbds",
                status_code=HTTPStatus.BAD_REQUEST,
                content=bytes(json.dumps({"detail": "invalid sbd fields"}), "utf-8"),
            )
            with pytest.raises(ConnectionError) as err:
                oda_helper.save(TestDataFactory.sbdefinition(sbd_id=None))

            assert len(mock_server.request_history) == 1
            assert (
                err.value.args[0]
                == "Response status 400 with body 'invalid sbd fields' while"
                f" sending post request to {TEST_ODA_URL}/sbds"
            )

    @mock.patch.dict(os.environ, {"ODA_URL": f"{TEST_ODA_URL}"})
    def test_sbd_id_not_found_handled(self):
        """
        Tests that a request to save an sbd_id that doesn't exist returns a KeyError
        """
        with requests_mock.Mocker() as mock_server:
            test_sbd = TestDataFactory.sbdefinition(sbd_id="sbd-test-1234")
            mock_server.put(
                f"{TEST_ODA_URL}/sbds/{test_sbd.sbd_id}",
                status_code=HTTPStatus.NOT_FOUND,
                content=bytes(
                    json.dumps({"detail": NOT_FOUND_MESSAGE("123")}),
                    "utf-8",
                ),
            )
            with pytest.raises(KeyError) as err:
                oda_helper.save(test_sbd)

            assert len(mock_server.request_history) == 1
            assert (
                err.value.args[0]
                == f"The requested identifier {test_sbd.sbd_id} could not be found. If"
                " you are trying to save a new SBDefintion, then the sbd_id field"
                " should be None and the ODA will generate the identifier"
            )

    @mock.patch.dict(os.environ, {"ODA_URL": f"{TEST_ODA_URL}"})
    def test_error_http_response_is_handled(self):
        """
        Tests that any error response from the API is raised as a ConnectionError
        """
        with requests_mock.Mocker() as mock_server:
            test_sbd = TestDataFactory.sbdefinition(sbd_id=None)
            mock_server.post(
                f"{TEST_ODA_URL}/sbds",
                status_code=HTTPStatus.INTERNAL_SERVER_ERROR,
                content=bytes(
                    json.dumps({"detail": "error returned from ODA"}), "utf-8"
                ),
            )
            with pytest.raises(ConnectionError) as err:
                oda_helper.save(test_sbd)

            assert len(mock_server.request_history) == 1
            assert (
                err.value.args[0]
                == "Response status 500 with body 'error returned from ODA' while"
                f" sending post request to {TEST_ODA_URL}/sbds"
            )

    @mock.patch.dict(os.environ, {"ODA_URL": f"{TEST_ODA_URL}"})
    def test_error_from_requests_is_handled(self):
        """
        Tests the path where the request.post call raises and error, which should be raised as a ConnectionError
        """
        with mock.patch("requests.post") as mock_post:
            mock_post.side_effect = HTTPError("requests.post raised error")
            with pytest.raises(ConnectionError) as err:
                oda_helper.save(TestDataFactory.sbdefinition(sbd_id=None))

            assert (
                err.value.args[0]
                == "Error 'HTTPError('requests.post raised error')' while sending post"
                f" request to {TEST_ODA_URL}/sbds"
            )
