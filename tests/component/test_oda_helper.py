"""
Tests of the ODA Helper module against a running instance of the ODA.

These will run from a test pod inside a kubernetes cluster, making requests
to a deployment of ska-db-oda in the same cluster.
"""

import os
from datetime import datetime

import pytest
from ska_oso_pdm import Metadata, OSOExecutionBlock, SBDefinition
from ska_oso_pdm._shared import TelescopeType
from ska_oso_pdm.execution_block import (
    PythonArguments,
    RequestResponse,
    ResponseWrapper,
)

from ska_db_oda.client.oda_helper import capture_request_response, create_eb, save
from ska_db_oda.client.odacli import EBClient, SBDClient
from ska_db_oda.persistence.domain import CODEC
from tests.conftest import ODA_URL, TestDataFactory

TEST_DATETIME = datetime.fromisoformat("2000-01-01T00:00:00.000000+00:00")


@capture_request_response
def dummy_function_to_decorate(first_param: int, second_param: str):
    return f"the test function is called with {first_param} and {second_param}"


@pytest.mark.post_deployment
@pytest.mark.xray("XTP-73522")
def test_create_then_update_eb():
    """
    Tests the scenario where the client is used to create an EB then a function
    that is decorated with the client decorator captures a request and response.
    """
    os.environ["ODA_URL"] = ODA_URL
    # Create the EB
    eb_id = create_eb(telescope=TelescopeType.SKA_MID)

    # Call the decorated function which should record the request_response
    dummy_function_to_decorate(1, second_param="test")

    # Assert against the complete EB stored in the ODA
    expected_eb = OSOExecutionBlock(
        eb_id=eb_id,
        telescope="ska_mid",
        metadata=Metadata(
            version=1,
            created_by="DefaultUser",
            created_on=TEST_DATETIME,
            last_modified_by="DefaultUser",
            last_modified_on=TEST_DATETIME,
        ),
        request_responses=[
            RequestResponse(
                request="tests.component.test_oda_helper.dummy_function_to_decorate",
                request_args=PythonArguments(args=[1], kwargs={"second_param": "test"}),
                request_sent_at=TEST_DATETIME,
                response_received_at=TEST_DATETIME,
                status="OK",
                response=ResponseWrapper(
                    result="'the test function is called with 1 and test'"
                ),
            )
        ],
    )

    eb_client = EBClient()

    result: OSOExecutionBlock = CODEC.loads(OSOExecutionBlock, eb_client.get(eb_id))

    # Ignore datetimes as can't mock the external running component.
    result.metadata.last_modified_on = TEST_DATETIME
    result.metadata.created_on = TEST_DATETIME
    result.request_responses[0].request_sent_at = TEST_DATETIME
    result.request_responses[0].response_received_at = TEST_DATETIME

    assert expected_eb == result


@pytest.mark.post_deployment
@pytest.mark.xray("XTP-73523")
def test_sbi_relationship_can_be_set():
    """
    Tests that an sbi_ref passed to the helper function persists the relationship in the EB
    """
    os.environ["ODA_URL"] = ODA_URL
    sbi_ref = "sbi-123"

    eb_id = create_eb(telescope=TelescopeType.SKA_MID, sbi_ref=sbi_ref)

    # Assert that the EB that has been created has the sbi_ref
    eb_client = EBClient()
    eb = eb_client.get(eb_id)
    result: OSOExecutionBlock = CODEC.loads(OSOExecutionBlock, eb)

    assert result.sbi_ref == sbi_ref


@pytest.mark.post_deployment
@pytest.mark.xray("XTP-73524")
def test_create_then_update_sbd():
    """
    Tests the scenario where the helper is used to persist a new SBDefinition
    then used to update it.
    """
    os.environ["ODA_URL"] = ODA_URL
    # Create the SBDefinition
    sbd = save(TestDataFactory.sbdefinition(sbd_id=None))

    # Update the SBDefinition
    sbd.interface = "a_new_interface"
    result = save(sbd)

    # Assert the value returned by the save function is the same as what exists in the database,
    sbd_client = SBDClient()
    persisted_sbd = CODEC.loads(SBDefinition, sbd_client.get(sbd.sbd_id))
    assert persisted_sbd == result
    assert result.metadata.version == 2
