import os

import pytest
import requests

from ska_db_oda.client._strategy import _get_type
from ska_db_oda.client.odacli import ODAFireUI
from ska_db_oda.persistence.domain import get_identifier
from ska_db_oda.persistence.unitofwork.filesystemunitofwork import FilesystemUnitOfWork
from tests.component import TEST_FILESYSTEM_IMPL
from tests.conftest import ODA_URL, TestDataFactory


def add_test_entity(resource, test_entity):
    return (
        add_test_entity_to_filesystem(resource, test_entity)
        if TEST_FILESYSTEM_IMPL
        else add_test_entity_to_api(resource, test_entity)
    )


def add_test_entity_to_api(resource, test_entity):
    entity_cls = _get_type(resource)
    entity_json = entity_cls.model_dump_json(test_entity)

    response = requests.post(
        f"{ODA_URL}/{resource}",
        data=entity_json,
        headers={"Content-type": "application/json"},
    )
    entity = entity_cls.model_validate_json(response.content)
    return get_identifier(entity)


def add_test_entity_to_filesystem(resource, test_entity):
    with FilesystemUnitOfWork() as uow:
        entity = getattr(uow, resource).add(test_entity)
        uow.commit()

    return get_identifier(entity)


@pytest.mark.parametrize(
    "resource,test_entity",
    [
        ("sbds", TestDataFactory.sbdefinition()),
        ("sbis", TestDataFactory.sbinstance()),
        ("prsls", TestDataFactory.proposal()),
        ("prjs", TestDataFactory.project()),
        ("ebs", TestDataFactory.executionblock()),
    ],
)
@pytest.mark.post_deployment
@pytest.mark.xray("XTP-73519")
def test_get_entity_using_client(resource, test_entity):
    os.environ["ODA_URL"] = ODA_URL
    os.environ["ODA_CLIENT_USE_FILESYSTEM"] = str(TEST_FILESYSTEM_IMPL)
    entity_id = add_test_entity(resource, test_entity)

    client = ODAFireUI()
    result = getattr(client, resource).get(entity_id)

    assert type(result) is str
    assert entity_id in result


@pytest.mark.parametrize(
    "resource,test_entity",
    [
        ("sbds", TestDataFactory.sbdefinition()),
        ("sbis", TestDataFactory.sbinstance()),
        ("prsls", TestDataFactory.proposal()),
        ("prjs", TestDataFactory.project()),
        ("ebs", TestDataFactory.executionblock()),
    ],
)
@pytest.mark.post_deployment
@pytest.mark.xray("XTP-73520")
def test_query_entity_using_client(resource, test_entity):
    os.environ["ODA_URL"] = ODA_URL
    os.environ["ODA_CLIENT_USE_FILESYSTEM"] = str(TEST_FILESYSTEM_IMPL)
    entity_id = add_test_entity(resource, test_entity)

    client = ODAFireUI()
    result = getattr(client, resource).query(user="DefaultUser")

    assert entity_id in result
