.venv\Scripts\isort --line-length 88 src/ tests/
.venv\Scripts\black --line-length 88 --exclude .+\.ipynb src/ tests/
.venv\Scripts\flake8 --max-line-length 88 src/ tests/
.venv\Scripts\pylint --max-line-length 88 src/ tests/
